#include <string>
#include <iostream>

using namespace std;

struct Pessoa
{
  string Identidade;
  string Nome;
  string Endereco;
  string Telefone;
};

int main()
{
  Pessoa clientes[2];

  for (int i = 0; i < 2; i++)
  {
    cout << "Nome: ";
    getline(cin, clientes[i].Nome);

    cout << "Identidade: ";
    getline(cin, clientes[i].Identidade);

    cout << "Endereço: ";
    getline(cin, clientes[i].Endereco);

    cout << "Telefone: ";
    getline(cin, clientes[i].Telefone);

    cout << endl;
  }

  cout << endl
       << "Clientes Cadastrados: \n\n";
  for (int i = 0; i < 2; i++)
  {
    cout << "-------------------------------" << endl
         << "Nome: " << clientes[i].Nome << endl
         << "Identidade: " << clientes[i].Identidade << endl
         << "Endereço: " << clientes[i].Endereco << endl
         << "Telefone: " << clientes[i].Telefone << "\n\n";
  }
  cout << endl;
}