#include <string>
#include <iostream>

using namespace std;

struct Pessoa
{
  string Matricula;
  string Nome;
  string Endereco;
  float MediaGeral;
};

int main()
{
  Pessoa alunos[5];

  for (int i = 0; i < 5; i++)
  {
    cout << "Nome: ";
    getline(cin, alunos[i].Nome);

    cout << "Matrícula: ";
    getline(cin, alunos[i].Matricula);

    cout << "Endereço: ";
    getline(cin, alunos[i].Endereco);

    cout << "Media Geral: ";
    cin >> alunos[i].MediaGeral;
    if (alunos[i].MediaGeral > 5)
      alunos[i].MediaGeral += 0.5;

    cin.ignore();
    cout << endl;
  }

  cout << endl
       << "Alunos" << endl;
  for (int i = 0; i < 5; i++)
  {
    cout << "Nome: " << alunos[i].Nome << endl
         << "Matrícula: " << alunos[i].Matricula << endl
         << "Endereço: " << alunos[i].Endereco << endl
         << "Média Final: " << alunos[i].MediaGeral << endl
         << "-------------------------------" << endl;
    cout << endl;
  }
  cout << endl;
}