#include <string>
#include <iostream>

using namespace std;

struct Pessoa
{
  string Nome;
  string corOlhos;
  string dataNascimento;
  float altura;
  float peso;
  int sexo;
};

int main()
{
  int n, contM = 0, contF = 0;
  cout << "Quantas pessoas deseja entrevistar? ";
  cin >> n;
  cin.ignore();
  Pessoa pessoas[n];

  for (int i = 0; i < n; i++)
  {

    cout << "Nome: ";
    getline(cin, pessoas[i].Nome);

    cout << "Cor dos seus olhos: ";
    getline(cin, pessoas[i].corOlhos);

    cout << "Data de nascimento: ";
    getline(cin, pessoas[i].dataNascimento);

    cout << "Altura: ";
    cin >> pessoas[i].altura;

    cout << "Peso: ";
    cin >> pessoas[i].peso;

    cout << "Sexo (0-F e 1-M): ";
    cin >> pessoas[i].sexo;
    if (pessoas[i].sexo == 0)
      contF++;
    else
      contM++;

    cin.ignore();
    cout << endl;
  }

  cout << "Mulheres: " << endl;
  for (int i = 0; i < n; i++)
  {
    if (pessoas[i].sexo == 0)
    {
      cout << "Nome: " << pessoas[i].Nome << endl
           << "Cor dos olhos: " << pessoas[i].corOlhos << endl
           << "Data de nascimento: " << pessoas[i].dataNascimento << endl
           << "Altura: " << pessoas[i].altura << endl
           << "Peso: " << pessoas[i].peso << endl;
    }
    cout << endl;
  }

  cout << endl
       << "Homens" << endl;
  for (int i = 0; i < n; i++)
  {
    if (pessoas[i].sexo == 1)
    {
      cout << "Nome: " << pessoas[i].Nome << endl
           << "Cor dos olhos: " << pessoas[i].corOlhos << endl
           << "Data de nascimento: " << pessoas[i].dataNascimento << endl
           << "Altura: " << pessoas[i].altura << endl
           << "Peso: " << pessoas[i].peso << endl;
    }
    cout << endl;
  }
}