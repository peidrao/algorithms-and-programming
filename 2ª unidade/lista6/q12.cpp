#include <string>
#include <iostream>

using namespace std;

struct tProduto
{
	string Descricao;
	float PrecoUnitario;
	float SubTotal;
	int Quantidade;
};

struct tNotaFiscal
{
	string Data;
	int Numero;
	float Total;
	tProduto itens[5];
};

void mostrarNotas(tNotaFiscal *notas);
void notaNumero(tNotaFiscal *notas);
void maiorValorTotal(tNotaFiscal *notas);
void totalUnidades(tNotaFiscal *notas);
void mediaTotal(tNotaFiscal *notas);

int main()
{
	tNotaFiscal NotasFiscais[5];
	int opc;

	for (int i = 0; i < 5; i++)
	{

		cout << "Nota Fiscal: " << endl;
		cout << "Data de hoje: ";
		getline(cin, NotasFiscais[i].Data);

		cout << "Número da nota: ";
		cin >> NotasFiscais[i].Numero;
		cin.ignore();

		cout << "Produtos contidos na nota: "
				 << "\n\n";
		for (int j = 0; j < 5; j++)
		{
			cout << "Descrição: ";
			getline(cin, NotasFiscais[i].itens[j].Descricao);

			cout << "Preço unitátio: ";
			cin >> NotasFiscais[i].itens[j].PrecoUnitario;

			cout << "Quantas unidades: ";
			cin >> NotasFiscais[i].itens[j].Quantidade;
			cin.ignore();
			cout << endl;

			NotasFiscais[i].itens[j].SubTotal = NotasFiscais[i].itens[j].Quantidade * NotasFiscais[i].itens[j].PrecoUnitario;
			NotasFiscais[i].Total += NotasFiscais[i].itens[j].SubTotal;
		}
	}

	system("clear");
	do
	{
		cout << "|-----------------------------------------|\n"
				 << "|             MENU PRINCIPAL              |\n"
				 << "|-----------------------------------------|\n"
				 << "| 1 - RELATÓRIO DE NOTAS FISCAIS          |\n"
				 << "| 2 - BUSCAR NOTA POR NÚMERO              |\n"
				 << "| 3 - EXIBIR NOTA COM MAIOR VALOR TOTAL   |\n"
				 << "| 4 - EXIBIR A QUANTIDADE TOTAL VENDIDA   |\n"
				 << "| DE UM PRODUTO A PARTIR DA SUA DESCRIÇÃO |\n"
				 << "| 5 - EXIBIR MÉDIA DO TOTAL DAS NOTAS     |\n"
				 << "| 6 - SAIR                                |\n"
				 << "|-----------------------------------------|\n\n";

		cout << "Escolha uma das opções: ";
		cin >> opc;

		switch (opc)
		{
		case 1:
			mostrarNotas(NotasFiscais);
			break;
		case 2:
			notaNumero(NotasFiscais);
			break;
		case 3:
			maiorValorTotal(NotasFiscais);
			break;
		case 4:
			totalUnidades(NotasFiscais);
			break;
		case 5:
			mediaTotal(NotasFiscais);
			break;
		case 6:
			cout << "Volte sempre :)" << endl;
			break;
		default:
			cout << "Você digitou uma opção inválida :(" << endl;
			break;
		}

	} while (opc != 6);
}

void mostrarNotas(tNotaFiscal *notas)
{
	system("clear");
	for (int i = 0; i < 5; i++)
	{
		cout << "|---------------------------|\n";
		cout << "Número da nota: " << notas[i].Numero << endl
				 << "Data: " << notas[i].Data << endl;
		for (int j = 0; j < 5; j++)
		{
			cout << "Produto: " << notas[i].itens[j].Descricao << endl
					 << "Preço unitário: " << notas[i].itens[j].PrecoUnitario << endl
					 << "Quantidade: " << notas[i].itens[j].Quantidade << endl
					 << "SubTotal: " << notas[i].itens[j].SubTotal << "\n\n";
		}
		cout << "Total: " << notas[i].Total << endl;
		cout << "|---------------------------|\n\n\n";
	}
}

void notaNumero(tNotaFiscal *notas)
{
	system("clear");
	int numeroNota;

	cout << "Digite o número da sua nota: ";
	cin >> numeroNota;

	for (int i = 0; i < 5; i++)
	{
		if (numeroNota == notas[i].Numero)
		{
			cout << "|---------------------------|\n";
			cout << "Número da nota: " << notas[i].Numero << endl
					 << "Data: " << notas[i].Data << endl;
			for (int j = 0; j < 5; j++)
			{
				cout << "Produto: " << notas[i].itens[j].Descricao << endl
						 << "Preço unitário: " << notas[i].itens[j].PrecoUnitario << endl
						 << "Quantidade: " << notas[i].itens[j].Quantidade << endl
						 << "SubTotal: " << notas[i].itens[j].SubTotal << "\n\n";
			}
			cout << "Total: " << notas[i].Total << endl;
			cout << "|---------------------------|\n\n\n";
		}
		else
		{
			cout << "Nota Não Encontrada!" << endl;
		}
	}
}

void maiorValorTotal(tNotaFiscal *notas)
{
	cin.ignore();
	int total = 0;

	for (int i = 0; i < 5; i++)
	{
		if (notas[i].Total > total)
			total = notas[i].Total;
	}

	for (int i = 0; i < 1; i++)
	{
		if (total == notas[i].Total)
		{
			cout << "|---------------------------|\n";
			cout << "Número da nota: " << notas[i].Numero << endl
					 << "Data: " << notas[i].Data << endl;
			for (int j = 0; j < 5; j++)
			{
				cout << "Produto: " << notas[i].itens[j].Descricao << endl
						 << "Preço unitário: " << notas[i].itens[j].PrecoUnitario << endl
						 << "Quantidade: " << notas[i].itens[j].Quantidade << endl
						 << "SubTotal: " << notas[i].itens[j].SubTotal << "\n\n";
			}
			cout << "Total: " << notas[i].Total << endl;
			cout << "|---------------------------|\n";
		}
	}
}

void totalUnidades(tNotaFiscal *notas)
{
	system("clear");
	cin.ignore();
	string descricaoProduto;
	cout << "Descrição do produto: ";
	getline(cin, descricaoProduto);
	int somatorio = 0;

	for (int i = 0; i < 5; i++)
	{

		for (int j = 0; j < 5; j++)
		{
			if (descricaoProduto == notas[i].itens[j].Descricao)
				somatorio += notas[i].itens[j].Quantidade;
		}
	}

	cout << "Foram vendidas " << somatorio << " unidades!" << endl;
}

void mediaTotal(tNotaFiscal *notas)
{
	system("clear");
	int mediaFinal = 0;
	for (int i = 0; i < 5; i++)
	{
		mediaFinal += notas[i].Total;
	}

	cout << "Valor total de todas as notas: R$ " << mediaFinal << endl;
}