#include <iostream>
#include <string>
using namespace std;

struct Pessoa
{
  string Nome;
  string CPF;
  string RG;
  string NumeroConta;
  string DataAberturaConta;
  float Saldo;
};

void cadastrarCliente(struct Pessoa *clientes, int i);
void encontrarCliente(struct Pessoa *clientes, int j);
void clientesNegativados(struct Pessoa *clientes, int j);

int main()
{
  Pessoa clientes[15];
  int opc, i = 0;
  system("clear");
  do
  {
    cout
        << "|------------ BANCO ------------|" << endl
        << "| 1 - Adicionar Cliente         |" << endl
        << "| 2 - Encontrar Cliente         |" << endl
        << "| 3 - Clientes c/ Saldo Negativo|" << endl
        << "|                               |" << endl
        << "| 0 - Sair                      |" << endl
        << "|-------------------------------|"
        << "\n\n";

    cout << "Digite uma opção: ";
    cin >> opc;
    cin.ignore();

    switch (opc)
    {
    case 1:
      cadastrarCliente(clientes, i);
      i++;
      break;
    case 2:
      encontrarCliente(clientes, i);
      break;
    case 3:
      clientesNegativados(clientes, i);
      break;
    case 0:
      cout << "\n\nVolte sempre! :)" << endl;
      break;
    default:
      cout << "Opção Inválida :(" << endl;
      break;
    }

  } while (opc != 0);
}

void cadastrarCliente(struct Pessoa *clientes, int i)
{
  system("clear");

  cout << "Nome: ";
  getline(cin, clientes[i].Nome);

  cout << "RG: ";
  getline(cin, clientes[i].RG);

  cout << "CPF: ";
  getline(cin, clientes[i].CPF);

  cout << "Número da conta: ";
  getline(cin, clientes[i].NumeroConta);

  cout << "Data de criação da conta: ";
  getline(cin, clientes[i].DataAberturaConta);

  cout << "Saldo: ";
  cin >> clientes[i].Saldo;
  cin.ignore();
}

void encontrarCliente(struct Pessoa *clientes, int j)
{
  system("clear");
  string cpfAux;
  cout << "Digitar o CPF do cliente: ";
  getline(cin, cpfAux);
  cout << "\n\n";
  for (int i = 0; i < j; i++)
  {
    if (cpfAux == clientes[i].CPF)
    {
      cout << "Nome: " << clientes[i].Nome << endl
           << "CPF: " << clientes[i].CPF << " | RG: " << clientes[i].RG << endl
           << "Número da Conta: " << clientes[i].NumeroConta << " | Data da abertura da conta: " << clientes[i].DataAberturaConta << endl
           << "Saldo: R$ " << clientes[i].Saldo << endl;
    }
  }
  cout << "\n\n";
}

void clientesNegativados(struct Pessoa *clientes, int j)
{
  system("clear");
  for (int i = 0; i < j; i++)
  {
    if (clientes[i].Saldo < 0)
    {
      cout << "Nome: " << clientes[i].Nome << endl
           << "CPF: " << clientes[i].CPF << " | RG: " << clientes[i].RG << endl
           << "Número da Conta: " << clientes[i].NumeroConta << " | Data da abertura da conta: " << clientes[i].DataAberturaConta << endl
           << "Saldo: R$ " << clientes[i].Saldo << endl;
    }
    cout << "\n\n";
  }
  cout << "\n\n";
}