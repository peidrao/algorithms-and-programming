#include <string>
#include <iostream>

using namespace std;

struct Pessoa
{
  string Codigo;
  string Email;
  int Horas;
  char Pagina;
  float Valor;
};

int main()
{
  Pessoa clientes[3];
  char opc;

  for (int i = 0; i < 3; i++)
  {
    cout << "Código: ";
    getline(cin, clientes[i].Codigo);
    cout << "Email: ";
    getline(cin, clientes[i].Email);
    cout << "Horas acessadas: ";
    cin >> clientes[i].Horas;
    if (clientes[i].Horas > 20)
    {
      int passou = clientes[i].Horas - 20;
      clientes[i].Valor = 35 + (passou * 2.5);
    }

    else
      clientes[i].Valor = 35;
    cout << "Tem Página? ";
    cin >> clientes[i].Pagina;
    if (clientes[i].Pagina == 'S')
      clientes[i].Valor += 40;
    cin.ignore();
    cout << endl;
  }

  for (int i = 0; i < 3; i++)
  {
    cout << "Cliente: " << clientes[i].Codigo << " | Valor a ser pago: R$ " << clientes[i].Valor << endl;
  }
}