#include <string>
#include <iostream>

using namespace std;

struct tAluno
{
  string nome;
  int matriculaDoAluno;
};

struct tDisciplina
{
  string disciplina;
  int matriculaAluno;
  float nota1;
  float nota2;
  float media;
};

void dadosAlunos(struct tAluno *alunos, struct tDisciplina *discplinas);
void alunoMaiorMedia(struct tAluno *alunos, struct tDisciplina *discplinas);
void mediaDasMedias(struct tAluno *alunos, struct tDisciplina *discplinas);
void alunoPorDisciplina(struct tAluno *alunos, struct tDisciplina *discplinas);

int main()
{
  tAluno alunos[3];
  tDisciplina disciplinas[12];
  int opc;

  for (int i = 0; i < 3; i++)
  {
    cout << "Nome do aluno: ";
    getline(cin, alunos[i].nome);
    cout << "Matricula do aluno: ";
    cin >> alunos[i].matriculaDoAluno;
    cin.ignore();
    cout << endl;
  }

  for (int i = 0; i < 12; i++)
  {
    cout << "Nome da discplina: ";
    getline(cin, disciplinas[i].disciplina);

    cout << "Aluno Matriculado: ";
    cin >> disciplinas[i].matriculaAluno;

    cout << "Nota 1: ";
    cin >> disciplinas[i].nota1;

    cout << "Nota 2: ";
    cin >> disciplinas[i].nota2;

    disciplinas[i].media = (disciplinas[i].nota1 + disciplinas[i].nota2) / 2;

    cin.ignore();
    cout << endl;
  }

  do
  {
    cout << "|-----------------------------------------|\n"
         << "|             MENU PRINCIPAL              |\n"
         << "|-----------------------------------------|\n"
         << "| 1 - EXIBIR DADOS DE UM ALUNO            |\n"
         << "| 2 - EXIBIR MAIOR MÉDIA                  |\n"
         << "| 3 - EXIBIR MÉDIAS DAS MÉDIAS POR ALUNO  |\n"
         << "| 4 - EXIBIR OS ALUNOS DE UMA DISCIPLINA  |\n"
         << "| 5 - SAIR                                |\n"
         << "|-----------------------------------------|\n\n";

    cout << "Escolha uma das opções: ";
    cin >> opc;

    switch (opc)
    {
    case 1:
      dadosAlunos(alunos, disciplinas);
      break;
    case 2:
      alunoMaiorMedia(alunos, disciplinas);
      break;
    case 3:
      mediaDasMedias(alunos, disciplinas);
      break;
    case 4:
      alunoPorDisciplina(alunos, disciplinas);
      break;
    case 5:
      cout << "Até logo :)" << endl;
      break;
    default:
      cout << "Você digitou uma opção inválida :(" << endl;
      break;
    }

  } while (opc != 5);
}

void dadosAlunos(struct tAluno *alunos,  tDisciplina *discplinas)
{
  system("clear");
  int matricula;
  cout << "Digite sua a matricula do aluno: ";
  cin >> matricula;

  for (int i = 0; i < 3; i++)
  {
    if (matricula == alunos[i].matriculaDoAluno)
    {

      cout << "Nome:" << alunos[i].nome << endl;
      for (int j = 0; j < 12; j++)
      {
        
        if (matricula == discplinas[j].matriculaAluno)
        {
          cout << "Disciplina: " << discplinas[j].disciplina << endl
               << "Nota 1 :" << discplinas[j].nota1 << endl
               << "Nota 2 :" << discplinas[j].nota2 << endl;
        }
      }
    }
  }
}

void alunoMaiorMedia(struct tAluno *alunos, struct tDisciplina *discplinas)
{
  system("clear");
  int nota = 0;
  int matricula;
  string materia;
  for (int i = 0; i < 3; i++)
  {
    if (discplinas[i].media > nota)
    {
      nota = discplinas[i].media;
      matricula = discplinas[i].matriculaAluno;
      materia = discplinas[i].disciplina;
    }
  }
  cout << "Aluno com maior Média" << endl;
  for (int i = 0; i < 12; i++)
  {
    if (matricula == alunos[i].matriculaDoAluno)
    {
      cout << "Nome: " << alunos[i].nome << endl
           << "Disciplina:" << materia << " | Média" << nota << endl;
    }
  }
}

void mediaDasMedias(struct tAluno *alunos, struct tDisciplina *discplinas)
{
  system("clear");
  float somatorio = 0;
  int matricula;

  cout << "Média das médias" << endl;
  for (int i = 0; i < 3; i++)
  {
    matricula = alunos[i].matriculaDoAluno;
    for (int j = 0; j < 12; j++)
    {
      if (matricula == discplinas[j].matriculaAluno)
        somatorio += discplinas[j].media;
    }
    cout << "Aluno: " << alunos[i].nome << endl
         << "Média: " << somatorio << endl;
    somatorio = 0;
    cout << endl;
  }
}

void alunoPorDisciplina(struct tAluno *alunos, struct tDisciplina *discplinas)
{
  system("clear");
  string materia;
  int matriculas;
  bool flag = false;
  cin.ignore();
  cout << "Qual a disciplina procurada: ";
  getline(cin, materia);

  for (int i = 0; i < 3; ++i){
    for (int j = 0; j < 12; ++j){
      if (materia == discplinas[j].disciplina)
          flag = true;
    }
    if(flag == true) 
      cout << "Alunos: " << alunos[i].nome << endl;
    flag = false;
  }



}
