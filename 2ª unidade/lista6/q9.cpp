#include <string>
#include <iostream>

using namespace std;

struct Endereco
{
	string Rua;
	string Bairro;
	string Cidade;
	string Estado;
	int numero;
};

struct Pessoa
{
	string Nome;
	string Telefone;
	struct Endereco Ender;
	int idade;
};

int main()
{
	Pessoa pessoas[10];
	string bairro;

	for (int i = 0; i < 10; i++)
	{
		cout << "Informações pessoais" << endl;
		cout << "Digite seu nome: ";
		getline(cin, pessoas[i].Nome);

		cout << "Digite seu telefone: ";
		getline(cin, pessoas[i].Telefone);

		cout << "Digite sua idade: ";
		cin >> pessoas[i].idade;
		cin.ignore();
		cout << "Informações de endereço" << endl;

		cout << "Estado (Sigla, ex. SP - São Paulo): ";
		getline(cin, pessoas[i].Ender.Estado);
		cout << "Cidade: ";
		getline(cin, pessoas[i].Ender.Cidade);
		cout << "Bairro: ";
		getline(cin, pessoas[i].Ender.Bairro);
		cout << "Rua: ";
		getline(cin, pessoas[i].Ender.Rua);
		cout << "Número da casa: ";
		cin >> pessoas[i].Ender.numero;
		cin.ignore();
		cout << endl;
	}

	cout << endl
			 << "Digite um bairro: ";

	getline(cin, bairro);

	for (int i = 0; i < 10; i++)
	{
		if (bairro == pessoas[i].Ender.Bairro)
			cout << pessoas[i].Nome << " mora neste bairro" << endl;
	}
	cout << endl;
}