#include <string>
#include <iostream>

using namespace std;

struct Pessoa
{
  string Identidade;
  string Nome;
  string Endereco;
  string Telefone;
};

void relatorioIndividual(struct Pessoa *clientes, int i)
{
  string identidade;
  cout << "Digite a identidade do usuário: ";
  getline(cin, identidade);
  for (int j = 0; j < i; j++)
  {
    if (identidade == clientes[j].Identidade)
    {
      cout << "Nome: " << clientes[j].Nome << endl
           << "Identidade: " << clientes[j].Identidade << endl
           << "Endereço: " << clientes[j].Endereco << endl
           << "Telefone: " << clientes[j].Telefone << "\n\n";
    }
    else
      cout << "Cliente não encontrado!" << endl;
  }
  cout << endl;
}

void mostrarRelatorio(struct Pessoa *clientes, int i)
{
  cout
      << endl
      << "Clientes Cadastrados: \n\n";
  for (int j = 0; j < i; j++)
  {
    cout << "Nome: " << clientes[j].Nome << endl
         << "Identidade: " << clientes[j].Identidade << endl
         << "Endereço: " << clientes[j].Endereco << endl
         << "Telefone: " << clientes[j].Telefone << "\n\n";
  }
  cout << endl;
}

int main()
{
  Pessoa clientes[20];
  char opc, opcFinal;
  int i = 0;

  do
  {
    cout << "Nome: ";
    getline(cin, clientes[i].Nome);

    cout << "Identidade: ";
    getline(cin, clientes[i].Identidade);

    cout << "Endereço: ";
    getline(cin, clientes[i].Endereco);

    cout << "Telefone: ";
    getline(cin, clientes[i].Telefone);

    cout << endl;
    i++;

    cout << "Deseja cadastrar mais algum usuário?";
    cin >> opc;
    cin.ignore();
  } while (i < 20 && opc != 'N');
  system("clear");

  cout << "Relatório (1), ou cadastro individual (2): ";
  cin >> opcFinal;
  cin.ignore();

  if (opcFinal == '1')
    mostrarRelatorio(clientes, i);
  else if (opcFinal == '2')
    relatorioIndividual(clientes, i);
}