#include <string>
#include <iostream>

using namespace std;

struct Pessoa
{
  string Nome;
  int Horas;
  int Dependentes;
  float SalarioBruto;
  float SalarioLiquido;
  float INSS;
  float IR;
  float ValorDescontado;
};

int main()
{
  Pessoa funcionarios[200];

  for (int i = 0; i < 200; i++)
  {
    cout << "Nome: ";
    getline(cin, funcionarios[i].Nome);

    cout << "Horas trabalhadas: ";
    cin >> funcionarios[i].Horas;
    funcionarios[i].SalarioBruto = 12 * funcionarios[i].Horas;
    cout << "Número de dependetes: ";
    cin >> funcionarios[i].Dependentes;
    if (funcionarios[i].Dependentes > 0)
      funcionarios[i].SalarioBruto += funcionarios[i].Dependentes * 40;

    funcionarios[i].INSS = funcionarios[i].SalarioBruto * 0.085;
    funcionarios[i].IR = funcionarios[i].SalarioBruto * 0.05;
    funcionarios[i].ValorDescontado = funcionarios[i].INSS + funcionarios[i].IR;
    funcionarios[i].SalarioLiquido = funcionarios[i].SalarioBruto - funcionarios[i].ValorDescontado;
    cin.ignore();
  }

  cout << endl
       << "Trabalhadores" << endl;
  for (int i = 0; i < 200; i++)
  {
    cout << "Nome: " << funcionarios[i].Nome << " | Salário Bruto: R$ " << funcionarios[i].SalarioBruto << endl
         << "Imposto de Renda: R$ " << funcionarios[i].IR << " | "
         << "INSS: R$ " << funcionarios[i].INSS << endl
         << "Salário Líquido: R$ " << funcionarios[i].SalarioLiquido << endl;
  }

  cout << endl;
}