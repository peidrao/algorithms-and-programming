#include <string>
#include <iostream>

using namespace std;

struct Pessoa
{
  string Identidade;
  string Nome;
  string Endereco;
  string Telefone;
};

int main()
{
  Pessoa clientes[20];
  char opc;
  int i = 0;

  do
  {
    cout << "Nome: ";
    getline(cin, clientes[i].Nome);

    cout << "Identidade: ";
    getline(cin, clientes[i].Identidade);

    cout << "Endereço: ";
    getline(cin, clientes[i].Endereco);

    cout << "Telefone: ";
    getline(cin, clientes[i].Telefone);

    cout << endl;
    i++;

    cout << "Deseja cadastrar mais algum usuário?";
    cin >> opc;
    cin.ignore();
  } while (i < 20 && opc != 'N');

  cout << endl
       << "Clientes Cadastrados: \n\n";
  for (int j = 0; j < i; j++)
  {
    cout << "-------------------------------" << endl
         << "Nome: " << clientes[j].Nome << endl
         << "Identidade: " << clientes[j].Identidade << endl
         << "Endereço: " << clientes[j].Endereco << endl
         << "Telefone: " << clientes[j].Telefone << "\n\n";
  }
  cout << endl;
}