#include <string>
#include <iostream>

using namespace std;

struct tAnimal
{
  string Nome;
  string Raca;
  int Idade;
};

int main()
{
  tAnimal animais[3];
  int idade = 0;
  string nome, raca;

  for (int i = 0; i < 3; i++)
  {
    cout << "Nome: ";
    getline(cin, animais[i].Nome);

    cout << "Raça: ";
    getline(cin, animais[i].Raca);

    cout << "Idade: ";
    cin >> animais[i].Idade;
    if (animais[i].Idade > idade)
    {
      idade = animais[i].Idade;
      nome = animais[i].Nome;
    }

    cin.ignore();
    cout << endl;
  }

  cout << "Deseja uma raça: ";
  getline(cin, raca);
  cout << endl;
  for (int i = 0; i < 3; i++)
  {
    if (raca == animais[i].Raca)
    {
      cout << "Nome: " << animais[i].Nome << " | "
           << "Idade: " << animais[i].Idade << endl;
    }
  }

  cout << endl
       << "O animal mais velho é o " << nome << " com " << idade << " anos." << endl;
}