/*
 Elabore um algoritmo que preencha automaticamente um vetor de 20 posições de inteiros gerando os
elementos a partir da multiplicação do seu índice por 2. Em seguida, calcule e escreva a soma dos elementos
pares e impares.
 */

#include <iostream>
#include <iomanip>

using namespace std;

int main() {

 	int vetor[20], somatorioPar = 0;
 	cout << "Vetor " << endl;
	for (int i=0; i<20; i++){
		vetor[i] = i * 2;
		if(vetor[i] % 2 == 0) somatorioPar += vetor[i];
		cout << vetor[i] << setw(3);
	}
	cout << endl << "O Somatório dos números pares é: " << somatorioPar << endl;
}