#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

	int matrizM[4][6], matrizN[4][6], matrizResultado[4][6];
	// Matrizes vão receber valores aleatórios entre 1 e 10 
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 6; j++){
			matrizM[i][j] = (rand() % 10 + 1);
			matrizN[i][j] = (rand() % 10 + 1);
		}
	}

	cout  << "Matriz M"	<< endl;
	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 6; j++){
		cout << setw(4) << matrizM[i][j] << setw(4)	;	
		}
		cout << endl;
	}

	cout << "\n\n" << "Matriz N"	<< endl;
	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 6; j++){
		cout << setw(4) << matrizN[i][j] 	;	
		}
		cout << endl;
	}

	cout << "\n\n" << "Produto de M e N" << endl;
	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 6; j++){
		cout << setw(4) << matrizM[i][j] * matrizN[i][j] ; 	
		}
		cout << endl;
	}

	cout << "\n\n" << "Soma de M com N" << endl;
	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 6; j++){
		cout << setw(4) << matrizM[i][j] + matrizN[i][j] ; 	
		}
		cout << endl;
	}

	cout << "\n\n" << "Diferença de M com N" << endl;
	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 6; j++){
		cout << setw(4) << matrizM[i][j] - matrizN[i][j] ; 	
		}
		cout << endl;
	}
}