#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

	int opc, vetor[15];

	for (int i = 0; i < 15; i++)
		vetor[i] = (rand() % 100 + 1); 

	cout << "Deseja ver números ímpares (1) ou pares (2): ";
	cin >> opc;

	if(opc == 1) {
		for (int i = 0; i < 15; i++)
				if(vetor[i] % 2 != 0) 
					cout << vetor[i] << setw(3);
	} else if (opc == 2) {
		for (int i = 0; i < 15; i++)
				if(vetor[i] % 2 == 0) 
					cout << vetor[i] << setw(3);
	}
	cout << endl;
}