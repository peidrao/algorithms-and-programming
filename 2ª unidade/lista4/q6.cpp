#include <iostream>
#include <ctime> 
#include <iomanip>
  
using namespace std;

int main() {
	srand(time(NULL));

	int vetorA[20], aux;

	cout << "Vetor Original" << endl;
	for (int i = 0; i < 20; i++) {
		vetorA[i] = (rand() % 100 + 1);
		cout << vetorA[i] << setw(3);
	}

	for (int i = 0; i < 10; i++){
		aux = vetorA[i];
		vetorA[i] = vetorA[19-i];
		vetorA[19-i] = aux;
	}

	cout << endl << "Vetor Transformado" << endl;
	for (int i = 0; i < 20; i++) 
	cout << vetorA[i] << setw(3);
		
	cout << endl;
}