#include <cstring>
#include <iostream>
 using namespace std;
 
char matriz[3][3];

void iniciarMatriz();
void mostrarMatriz();
void jogador(char simbolo);
int quadrante();
int vazio();


int main()
{
	char sim[2];
	int vez = 1;
	
	do
	{
	
		//int vez = 1;
		char simbolo;
		char nome1[21], nome2[21];
		char *c;
		int jogada = 0;
		
		cout << "=====================\n";
		cout << "==| JOGO DA VELHA |==\n";
		cout << "=====================\n\n";

		cout << "NOME DO JOGADOR 1: ";
		cin >> nome1;

		cout << "NOME DO JOGADOR 2: ";
		cin >> nome2;

		iniciarMatriz();

		do
		{
			
			mostrarMatriz();

			cout << endl;

			if (vez)
			{
				cout << "Vez do jogador: " << nome1;
				simbolo = 'X';
				jogador(simbolo);
				vez = 0;
			}
			else
			{
				cout << "Vez do jogador: " << nome2;
				simbolo = 'O';
				jogador(simbolo);
				vez = 1;
			}


			if (vazio())
			{
				if (vez)
				{


					mostrarMatriz();
					cout << endl;
					cout << "O jogador " << nome2 << " ganho!" << endl;
					vez = 0;
					break;
				}
else
				{
					
					mostrarMatriz();
					cout << endl;
					cout << "O jogador " << nome1 << " ganho!" << endl;
					vez = 1;
					break;
				}
			}
			else if (quadrante()){
				mostrarMatriz();
				cout << endl;
				cout << "O jogo terminou empatado!\n\n";
				break;
			}

			jogada++;

		} while (1);

		cout << "\nDeseja jogar novamente? s/n: ";
		cin >> sim;
		cout << endl;

		

	} while (!strcmp(sim, "s"));

	cout << "voce saiu do Jogo!\n\n";

    return 0;
}



void iniciarMatriz()
{
	int i;
	int j;

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			matriz[i][j] = ' ';
		}
	}
}



void mostrarMatriz()
{
	int i;

	cout << endl;

	for (i = 0; i < 3; i++)
	{
		cout << matriz[i][0]  << " |" <<  matriz[i][1] << " |" <<  matriz[i][2];
		if (i < 2)
		{
			cout << "\n--|--|---\n";
		}
	}
	cout << "\n\n";

}



void jogador(char simbolo)
{
	int i;
	int j;
	int jogada = 0;

	cout << endl;

	
	do {
		do
		{
			cout << "LINHA: ";
			cin >> i;

			if (i > 3 || i < 1){
				cout << endl;
				cout << "ERRO: JOGADA INVALIDA!\n";
				cout << endl;
				jogada = 0;
			}

			else
			{
				jogada = 1;
			}
		} while (jogada == 0);

		do
		{
			cout << "COLUNA: ";
			cin >> j;

			if (j > 3 || j < 1)
			{
				cout << endl;
				cout << "ERRO: JOGADA INVALIDA!\n";
				cout << endl;
				jogada = 0;
			}
			else{
				jogada = 1;
			}
		
		} while (jogada == 0);

		if (matriz[i - 1][j - 1] != ' ')
		{
			cout << endl;
			cout << "ERRO: ESSA JOGADA JA FOI EFETUADA!\n";
			cout << endl;
			continue;
		}

	} while (matriz[i - 1][j - 1] != ' ');
	
	matriz[i - 1][j - 1] = simbolo;

}

int quadrante()
{
	int i;
	int j;

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			if (matriz[i][j] == ' ')
			{
				return 0;
			}

		}
	}
	return 1;
}

int vazio()
{
	// Linhas
	if (matriz[0][0] == matriz[0][1] && matriz[0][1] == matriz[0][2] && matriz[0][0] != ' ')
	{
		return 1;
	}
	if (matriz[1][0] == matriz[1][1] && matriz[1][1] == matriz[1][2] && matriz[1][0] != ' ')
	{
		return 1;
	}
	if (matriz[2][0] == matriz[2][1] && matriz[2][1] == matriz[2][2] && matriz[2][0] != ' ')
	{
		return 1;
	}
	// Colunas
	if (matriz[0][0] == matriz[1][0] && matriz[1][0] == matriz[2][0] && matriz[0][0] != ' ')
	{
		return 1;
	}
	if (matriz[0][1] == matriz[1][1] && matriz[1][1] == matriz[2][1] && matriz[0][1] != ' ')
	{
		return 1;
	}
	if (matriz[0][2] == matriz[1][2] && matriz[1][2] == matriz[2][2] && matriz[0][2] != ' ')
	{
		return 1;
	}
	// Diagonais
	if (matriz[0][0] == matriz[1][1] && matriz[1][1] == matriz[2][2] && matriz[0][0] != ' ')
	{
		return 1;
	}
	if (matriz[2][0] == matriz[1][1] && matriz[1][1] == matriz[0][2] && matriz[2][0] != ' ')
	{
		return 1;
	}
	return 0;
}
