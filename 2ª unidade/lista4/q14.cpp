#include <iostream>
#include <ctime> 
#include <iomanip>
 
using namespace std;

int verificar(int vetor[], int inicio, int final){ 
    if (inicio >= final) return 1; 
    if (vetor[inicio] == vetor[final]) return verificar(vetor, inicio + 1, final - 1);  
    else return 0; 
} 
  
int main() 
{ 
	srand(time(NULL));

	int vetor[20];

	for (int i = 0; i < 20; i++) {
		cin >> vetor[i];
	}

	// Calcular tamanho do vetor;
    int n = sizeof(vetor) / sizeof(vetor[0]); 
  
    if (verificar(vetor, 0, n - 1) == 1) 
        cout << "Palíndromo"; 
    else
        cout << "Não é palíndromo"; 
  
    return 0; 
} 