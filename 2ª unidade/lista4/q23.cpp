#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

 	int M[30][30], a;
 	cout << "Digite um valor: ";
 	cin >> a;

 	for (int i = 0; i < 30; i++){
 		for (int j = 0; j < 30; j++){
 			M[i][j] = (rand() % 10 + 1);
 			if(M[i][j] == a) M[i][j] = 0;
 		}
 	}

 	for (int i = 0; i < 30; i++){
 		for (int j = 0; j < 30; j++){
 			cout << setw(4) << M[i][j];
 		}
 		cout << endl;
 	}
 	cout << endl;
}


