#include <iostream>
#include <ctime> 
#include <iomanip>
  
using namespace std;

int main() {
	srand(time(NULL));

	int vetorA[20], vetorB[20], aux;

	for (int i = 0; i < 20; i++)
		vetorA[i] = (rand() % 100 + 1); 

	for (int i = 0; i < 10; i++){
		aux = vetorA[i];
		vetorB[i] = vetorA[19-i];
		vetorB[19-i] = aux;
	}

	cout << "Vetor Original" << endl;
	for (int i = 0; i < 20; i++) 
	cout << vetorA[i] << setw(3);
	
	cout << endl << "Vetor Transformado" << endl;
	for (int i = 0; i < 20; i++) 
	cout << vetorB[i] << setw(3);
		
	cout << endl;
}