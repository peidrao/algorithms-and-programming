#include <iostream>
#include <ctime> 

using namespace std;

int main() {
	srand(time(NULL));

	int vetorA[10], vetorB[10], vetorC[10];

	for (int i = 0; i < 10; i++) {
		vetorA[i] = (rand() % 100 + 1);
		vetorB[i] = (rand() % 100 + 1);
		vetorC[i] = vetorA[i] * vetorB[i];
		cout << vetorA[i] << " x " << vetorB[i] << " = " << vetorC[i] << endl;
	}
}