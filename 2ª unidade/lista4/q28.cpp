#include <iostream>
#include <iomanip>
  
using namespace std;

int main() {
	int M[6][6], aux;
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 6; j++){
			if (i == 0 || i == 5 || j == 0 || j == 5) cout << setw(3) << '1';
			else if((i == 2 && j == 2) || (i == 2 && j == 3) || (i == 3 && j == 2) || (i == 3 && j == 3)) cout << setw(3) << '3';
			else cout << setw(3) << '2';
		}
	cout << endl;
	}

	cout << endl;
}