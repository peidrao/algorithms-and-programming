#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));
	int S[10]; // Como quero valores aleatórios, irei gerar um vetor com números aleatórios, sem precisar de condição de parada.
	int reajuste = 0;
	cout << "Salários" << endl;
	for (int i = 0; i < 10; i++) {
		S[i] = (rand() % 1000+ 1);
		cout << S[i] << setw(4);
	}

	cout << "\n\n" << "Reajuste salarial: " ;
	cin >> reajuste;

	for (int i = 0; i < 10; ++i)
		S[i] += reajuste;

	cout << "Salários com  reajuste"<< endl;
	for (int i = 0; i < 10; i++)
		cout << S[i] << setw(4);

	cout << endl;
}