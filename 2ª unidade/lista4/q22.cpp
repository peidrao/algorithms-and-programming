#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

 	int M[6][6], V[36], a;
 	cout << "Digite um valor: ";
 	cin >> a;

 	for (int i = 0; i < 6; i++){
 		for (int j = 0; j < 6; j++){
 			M[i][j] = (rand() % 10 + 1);
 		}
 	}

 	for (int i = 0; i < 6; i++){
 		for (int j = 0; j < 6; j++){
 			V[i*j] = a * M[i][j];
 			cout << V[i*j] << setw(4);
 		}
 	}
 	cout << endl;
}


