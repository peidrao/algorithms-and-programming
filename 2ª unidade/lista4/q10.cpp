#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

 	int A[10], B[10], C[10], somatorioA = 0;

	for (int i=0; i<10; i++){
		A[i] = (rand() % 10 + 1);
		somatorioA += A[i];
		B[i] = (rand() % 10 + 1);
		C[i] = A[i] + B[i];
	}

	cout << "VETOR A" << endl;
	for (int i = 0; i < 10; ++i)
		cout << A[i]  << setw(4);
	cout << "\n\n";

	cout << "VETOR B" << endl;
	for (int i = 0; i < 10; ++i)
		cout << B[i]  << setw(4);
	cout << "\n\n";

	cout << "VETOR C" << endl;
	for (int i = 0; i < 10; ++i)
		cout << C[i]  << setw(4);
	cout << "\n\n";

	cout << "Soma dos elementos de A: " << somatorioA << endl;

}