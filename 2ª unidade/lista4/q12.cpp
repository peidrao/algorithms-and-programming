
#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

 	int A[10], num;
	// Não fiz verificação, pois é simples e nesse caso não é necessário
	for (int i=0; i<10; i++){
		A[i] = (rand() % 10 + 1);
	}

	cout << "Digite um número: ";
	cin >> num;
	
	for (int i = 0; i < 10; i++){
		if(A[i] == num) cout << "Número encontrado!" << endl;	
	}
}