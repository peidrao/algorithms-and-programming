#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

 	int M[12][13], V[12], aux = 0;

 	for (int i = 0; i < 12; i++){
 		for (int j = 0; j < 13; j++){
 			M[i][j] = (rand() % 100 + 1);
 		}
  }

  for (int i = 0; i < 12; i++){
    for (int j = 0; j < 13; j++){
        if(M[i][j] > aux) {
          aux = M[i][j];
          V[i] = M[i][j];
        }
    }
    aux = 0;
  }
  cout << "\n\n";
  for (int i = 0; i < 12; i++){
    for (int j = 0; j < 13; j++){
    cout << setw(7) <<  M[i][j] *  V[i] ;
    }
    cout << endl;
  }
  cout << endl;

}


