#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	int gabarito[13] = {1, 3, 2, 2, 1, 3, 1, 2, 3, 1, 1, 2, 3};
	int apostador;
	int resposta[13];
	int contador = 0;

	do {
		cout << "Digite o número do seu cartão: ";
		cin >> apostador;

		if(apostador > 0) {
			cout << "Digite sua aposta: " << endl;
			for (int i = 0; i < 13; i++){
				cout << i+1 << ": ";
				cin >> resposta[i];

				if(resposta[i] == gabarito[i]) contador++;
				if(contador == 13) cout << "Ganhador!" << endl;
			}
		}
		cout << endl;
	} while (apostador > 0);
}

