#include <iostream>
#include <ctime> 
#include <iomanip>
  
using namespace std;

int main() {
	srand(time(NULL));

	int numero, x, aux;
	cout<< "Digite um número: ";
	cin >> numero;
	int vetor[numero];
	vetor[0] = numero;
	cout << "Vetor Original" << endl;
	for (int i = 1; i < numero; i++) {
		vetor[i] = vetor[i-1] * 2; 
	}

	for (int i = 0; i < numero; i++)
		cout << vetor[i] << setw(4);

	cout << "\n\n" << "Busque um númro no vetor: " ;
	cin >> x;
	aux = vetor[0];
	for (int i = 0; i < numero; i++){
		if(x == vetor[i]) {
			vetor[i] = aux;
			vetor[0] = x;
		}
	}

	cout << "Vetor Mudado" << endl;
	for (int i = 0; i < numero; i++)
		cout << vetor[i] << setw(4);
		
	cout << endl;
}