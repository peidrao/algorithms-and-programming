#include <iostream>
#include <ctime> 

using namespace std;

int main() {
	srand(time(NULL));
	int n, somatorio = 0, media = 0;
	cout << "Tamanho do vetor: ";
	cin >> n;

	int M[n], N[n];
	for (int i = 0; i < n; i++) {
		M[i] = (rand() % 100 + 1);
		N[i] = (rand() % 10 + 1);
		somatorio += N[i];
	}
	media = somatorio / n;
	cout << endl << "Média: " << media << "\n\n";

	cout << "Notas maiores que a média" << endl;
	for (int i = 0; i < n; ++i)
		if(N[i] > media) cout << N[i] << endl;

	cout << endl;
	cout << "Matrículas dos alunos com notas menores que a média" << endl;
	for (int i = 0; i < n; ++i)
		if(N[i] < media) cout << M[i] << endl;
	cout << endl;
}