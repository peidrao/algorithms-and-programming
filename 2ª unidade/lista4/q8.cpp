#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

 	int vetorA[10], vetorB[12];
 	cout << "Vetor A" << endl;
	for (int i=0; i<10; i++){
		vetorA[i] = (rand() % 10 + 1);
		cout << vetorA[i] << setw(3);
	}
	cout << endl << "Vetor B" << endl;
	for (int i=0; i<12; i++){
		vetorB[i] = (rand() % 10 + 1);
		cout << vetorB[i] << setw(3); 
	}
	cout << endl << "Elementos comuns entre os vetores" << endl;
	for (int i = 0; i < 10; i++){
		for (int j = 0; j < 12; j++){
			if (vetorA[i]==vetorB[j])
				 cout << vetorB[j] << setw(3) ;
		}
	}

	cout << endl;
}