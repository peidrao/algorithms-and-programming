#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

 	int M[10][10];
 	int P[10], S[10], L5[10], C10[10], coluna = 0, linha = 0;

 	cout << "Matriz original" << endl;
 	for (int i = 0; i < 10; i++){
 		for (int j = 0; j < 10; j++){
 			M[i][j] = (rand() % 100 + 1);
 			cout << setw(5) << M[i][j]; 
 			
 			if(i == j) P[i] = M[i][j]; // Pegar elementos da diagonal primaria;
 			if(i == 4) {L5[linha] = M[i][j]; linha++; }
 			if(j == 9) {C10[coluna] = M[i][j]; coluna++; }
 			if(i + j == 9) S[i] = M[i][j];
 					
 		}
 		cout << endl;
}

	coluna = 0; linha = 0;
	for (int i = 0; i < 10; i++){
 		for (int j = 0; j < 10; j++){
 			if(i == j) M[i][j] = S[i] ; // Pegar elementos da diagonal Secundária;
 			if(i == 4) { M[i][j] = C10[coluna] ; coluna++; }
 			if(j == 9) { M[i][j] = L5[linha]; linha++; } 
 			if(i + j == 9)  M[i][j] = P[i] ;
 		}
}

	cout << "\n\nMatriz modificada" << endl;
 	for (int i = 0; i < 10; i++){
 		for (int j = 0; j < 10; j++){
 			cout << setw(5) << M[i][j]; 
 		}
 		cout << endl;
}

}