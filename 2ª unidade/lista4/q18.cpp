#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;
// Ordem aleatório já é estabelecida a partir do rand()
int main() {
	srand(time(NULL));
	int S[20], aux; 
	
	for (int i = 0; i < 20; i++) {
		S[i] = (rand() % 100 + 1);
	}

	for(int i=0 ; i<20; i++){		
		for(int j=i+1 ; j<20 ; j++){
			if(S[i]>S[j]){
				aux  =S[i];
				S[i]=S[j];
				S[j]=aux;
			}
		}
	}
	
	cout << "\nOrdem decrescente: " << endl;
	for (int i = 0; i < 20; i++) 
		cout << S[i] << setw(4);

	cout << "\nOrdem decrescente: " << endl;
	for (int i = 19; i >= 0; i--) 
		cout << S[i] << setw(4);
	cout << endl;
}