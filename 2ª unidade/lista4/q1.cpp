#include <iostream>
#include <ctime> 
using namespace std;

int main() {
	srand(time(NULL));

	int opc, vetor[20];

	for (int i = 0; i < 20; i++)
		vetor[i] = (rand() % 100 + 1); 

	cout << "Ordem direra (1) ou inversa (2): ";
	cin >> opc;

	if(opc == 1) {
		for (int i = 0; i < 20; i++)
				cout << vetor[i] << " " ;
	} else if (opc == 2) {
		for (int i = 19; i >= 0 ; i--) 
			cout << vetor[i] << " " ;	
		} else {
			cout << "Opção inválida!" << endl;
		}

		cout << endl;
}