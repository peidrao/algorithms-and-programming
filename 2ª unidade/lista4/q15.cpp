#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	
	bool A[6], B[6], R[6];
	char operador;

	for (int i = 0; i < 6; i++) {
		cout << "Vetor A: ";
		cin >> A[i];
		cout << "Vetor B: ";
		cin >> B[i];
	}
	cout << endl << "Digite um operador (&) = e (|) ou: ";
	cin >> operador;

		switch(operador) {
			case '&':
				for (int i = 0; i < 6; i++){
					if((A[i] == B[i]) && (A[i] == 1) && (B[i] == 1)) R[i] = 1;
					else R[i] = 0;
				}
			break;
			case '|':
				for (int i = 0; i < 6; i++){
					if((A[i] == B[i]) && (A[i] == 0) && (B[i] == 0)) R[i] = 0;
					else R[i] = 1;
				}
			break;
			default:
			cout << "Operador inválido!";
		}

		cout << endl << "Vetor A" << endl;
		for (int i = 0; i < 6; i++)
			cout << A[i] << setw(4);
	
		cout << endl << "Vetor B" << endl;
		for (int i = 0; i < 6; i++)
			cout << B[i] << setw(4);

		cout << endl << "Vetor R" << endl;
		for (int i = 0; i < 6; i++)
			cout << R[i] << setw(4);

cout << endl;
}