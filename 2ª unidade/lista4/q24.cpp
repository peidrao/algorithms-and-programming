#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

 	int M[5][5], a = 0, b = 0, c = 0, d =0, e = 0;
 	cout << endl;
 	for (int i = 0; i < 5; i++){
 		for (int j = 0; j < 5; j++){
 			M[i][j] = (rand() % 10 + 1);
 			e += M[i][j];
 			if(i == 3)  a += M[i][j]; 
 			if(j == 1) b += M[i][j];
 			if((i == 0 && j == 0) || (i == 1 && j == 1) || (i == 2 && j == 2) || (i == 3 && j == 3) || (i == 4 && j == 4) ) c += M[i][j];
 			if((i == 0 && j == 4) || (i == 1 && j == 3) || (i == 2 && j == 2) || (i == 3 && j == 1) || (i == 4 && j == 0) ) d += M[i][j];
 		}
 	}

 	for (int i = 0; i < 5; i++){
 		for (int j = 0; j < 5; j++){
 			cout << setw(4) << M[i][j];
 		}
 		cout << endl;
 	}
 	cout << endl << "Soma da linha 4: " << a << endl
 				 <<  "Soma da coluna 2: " << b << endl
 				 << "Soma da Diagonal Primária: " << c << endl
 				 <<	"Soma da Diagonal Secundária: " << d << endl
 				 << "Soma de todos os elementos da matriz: " << e << endl;
}
