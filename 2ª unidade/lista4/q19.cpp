/*
 Faça um programa que declare dois vetores A e B de 10 elementos de 
 inteiros, leia os seus elementos e
intercale os dois vetores A e B formando o vetor C (de 20 elementos).

 */

#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

 	int vetorA[10], vetorB[10], vetorC[20];
 	int p = 0;
	for (int i=0; i<10; i++){
		vetorA[i] = (rand() % 10 + 1);
		vetorB[i] = (rand() % 10 + 1);
		
	}
	cout << endl << "Vetor A" << endl;
	for (int i = 0; i < 10; ++i)
		cout << vetorA[i] << setw(3);

	cout   << endl << "Vetor B" << endl;
	for (int i = 0; i < 10; ++i)
		cout << vetorB[i] << setw(3);

	for (int i=0; i<10; i++){
		vetorC[p] = vetorA[i];
		p += 2;
	}
	p = 1;
	for (int i=0; i<10; i++){
		vetorC[p] = vetorB[i];
		p += 2;
	}

	cout << endl << "Vetor C" << endl;
	for (int i = 0; i < 20; i++)
		cout << vetorC[i] << setw(4);

	cout << endl;
}