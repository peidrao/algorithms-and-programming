#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));
	int vetor[20];

	for (int i = 0; i < 20; i++) {
		vetor[i] = (rand() % 100 + 1);
		cout << setw(3) << vetor[i];
	}
	cout << endl;
	for (int i = 0; i < 20; i++){
		if(vetor[i] % 2 == 0) 
			vetor[i] = 0;
		cout << setw(3)  << vetor[i];
	}
cout << endl;
}