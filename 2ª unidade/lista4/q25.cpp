/*
 Escreva um algoritmo que leia uma matriz A(15,5) e a escreva. Verifique, a seguir, quais os elementos de A
que estão repetidos e quantas vezes cada um está repetido. Escrever cada elemento repetido com uma
mensagem dizendo quantas vezes cada elemento aparece em A.
 */


#include <iostream>
#include <ctime> 
#include <iomanip>

using namespace std;

int main() {
	srand(time(NULL));

 	int A[15][5], aux, repeticoes =0;

 	for (int i = 0; i < 15; i++){
 		for (int j = 0; j < 5; j++){
 			A[i][j] = (rand() % 10 + 1);
 			//cout << A[i][j] << setw(4);

        	aux = A[i][j];
        	for (int k = 0; k < 15; k++){
            	for (int l = 0; l < 5; l++){
                	if (aux == A[k][l]) repeticoes++;
                }
            }
            cout << "Número repetido: " << aux << "| Vezes repetidas: " << repeticoes << endl; 
      		repeticoes = 0;
      	}

 	}

}


