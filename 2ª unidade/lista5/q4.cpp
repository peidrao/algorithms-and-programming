#include <iostream>

using namespace std;

int main() {
	string frase;
	char t;
	getline(cin, frase);
	
	int p = frase.length() - 1;
    for(int i=0; i<frase.length() / 2; i++){
        t = frase[i];
        frase[i] = frase[p];
        frase[p] = t;
        p--;
    }
    cout << frase << endl;

}
