#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
     string str1, str2;
     cout << "Digite alguma coisa: ";  
     getline(cin,str1);   

    for (auto & c: str1) {
    	c = toupper(c);
    	str2 += c;
    }
    cout << "Primeira string: " << str1 << endl;
    cout << "Segunda string: " << str2 << endl;
}
