#include <iostream>
#include <string>

using namespace std;

int main() {
	string frase, palavra;
	int posicao = 0;

	getline(cin, frase);
	getline(cin, palavra);

	posicao = frase.find(palavra);

	if(posicao == -1) cout << "A palavra " << palavra << " não está contida na frase!" << endl;
	else cout << "A palavra “frase” está contida na frase “esta é a frase”." << endl;

}
