#include <iostream>
#include <string>
#include <ctime>

using namespace std;

struct Animais {
	string nomeAnimal;
	string especieAnimal;
	int idadeAnimal;
	long int codigoAnimal; 
	char sexoAnimal;
};

struct Proprietario {
	string nome;
	long int CPF; //chave de busca
	int idade;
	int quantidadeAnimais;
	Animais animal[4];
};

void listarProprietarios(Proprietario *proprietarios, int n); /* Listar todos os proprietários com seus respectivos animais */
void procurarProprietario(Proprietario *proprietarios, int n);  /* Procurar e listar um determinado proprietário pelo seu CPF */
void editarDeletar(Proprietario *proprietarios, int n); /* Editar ou remover um proprietário pelo seu CPF */
void procuraAnimais(Proprietario *proprietarios, int n); /* Procurar animais de estimação pela sua espécie */
void editarAnimal(Proprietario *proprietarios, int n); /* Editar o nome de um animal pelo seu código */
void deletarAnimais(Proprietario *proprietarios, int n); /* Excluir todos os animais de um proprietário */

int main()
{
	srand(time(NULL));
	system("cls")||system("clear");
	int opc, n;

	cout    << "+---------------------------------------------+\n"
			<< "|   DESEJA ADICIONAR QUANTOS PROPRIETÁRIOS    |\n"
			<< "+---------------------------------------------+\n"
			<< "|\n"
			<< "|\n"
			<< "|>>>> ";
	cin >> n;
	getchar();
	Proprietario proprietarios[n]; /* Definição de quantos elementos teremos em nosso registro principal */

	for (int i = 0; i < n; i++) {
		bool cpfExiste = false;
		int auxCPF; // Variável que recebe um CPF
		cout << "Nome do Proprietario: ";
		getline(cin, proprietarios[i].nome);
		
		do {
			int contador = 0;
			cout << "CPF do Proprietário: ";
			cin >> auxCPF ;

			for (int k = 0; k < n; k++) {
				if (auxCPF == proprietarios[k].CPF) 
					contador++;
			}

			if (contador > 0) cout << "CPF EXISTE\n";
			if (contador == 0) cpfExiste = true;
		} while (cpfExiste != true);

		proprietarios[i].CPF = auxCPF;

		cout << "Idade do proprietário: ";
		cin >> proprietarios[i].idade;

		cout << "Quantidade de PET's: ";
		cin >> proprietarios[i].quantidadeAnimais;
		getchar();

		cout << "\nInformações dos animal: \n\n";
		for (int j = 0; j < proprietarios[i].quantidadeAnimais; j++) {
			cout << "Nome do PET: ";
			getline(cin, proprietarios[i].animal[j].nomeAnimal);

			cout << "Espécie de animal: ";
			getline(cin, proprietarios[i].animal[j].especieAnimal);
			
			proprietarios[i].animal[j].codigoAnimal =  (rand() % 1000 + j);

			cout << "Idade do animal: ";
			cin >> proprietarios[i].animal[j].idadeAnimal;
			getchar();

			cout << "Sexo do animal (M/F): ";
			cin >> proprietarios[i].animal[j].sexoAnimal;
			getchar();
			cout << "\n\n";
		}
		cout << "\n\n";
	}
	system("cls")||system("clear");

	do {
		cout << "+========================================================+\n"
			 << "|                 MENU DO PROPRIETÁRIO                   |\n"
			 << "+========================================================+\n"
			 << "|  [1] - LISTAR TODOS OS PROPRIETÁRIOS C/ SEUS ANIMAIS   |\n"
			 << "|  [2] - PROCURAR PROPRIETÁRIO              	          |\n"
			 << "|  [3] - EDITAR OU EXCLUIR PROPRIETÁRIO                  |\n"
			 << "|  [4] - PROCURAR ANIMAIS                                 |\n"
			 << "|  [5] - EDITAR ANIMAL                                    |\n"
			 << "|  [6] - DELETAR ANIMAL                                   |\n"
			 << "+========================================================+\n"
			 << "|  [0] - SAIR                                             |\n"
			 << "+========================================================+\n"
			 << "+\n"
			 << "=>>> ESCOLHA UMA OPÇÃO >>> ";
		cin >> opc;

		switch (opc){
		case 0:
			cout << "Até logo :)" << endl;
			break;
		case 1:
			listarProprietarios(proprietarios, n);
			break;
		case 2:
			procurarProprietario(proprietarios, n);
			break;
		case 3:
			editarDeletar(proprietarios,n);
			break;
		case 4: 
			procuraAnimais(proprietarios, n);
			break;
		case 5: 
		editarAnimal(proprietarios,n);
			break;
		case 6: 
		deletarAnimais(proprietarios,n);
			break;
		default:
			cout << "Opção inválida :(\n\n\n";
		}
	} while (opc != 0);
}

void listarProprietarios(Proprietario *proprietarios, int n) {
	system("cls")||system("clear");
	int contadorProp = 0;
	for (int i = 0; i < n; ++i) {
		if(proprietarios[i].CPF == 0) contadorProp++;
		if(proprietarios[i].CPF != 0) {
			cout << "------------------------------------------------\n"
			 << "PROPRIETÁRIO [" << i + 1 << "]: " << proprietarios[i].nome  << " | Idade: " << proprietarios[i].idade << endl
			 << "CPF: " << proprietarios[i].CPF << " | TOTAL DE ANIMAIS: " <<  proprietarios[i].quantidadeAnimais << endl
			 << "INFORMAÇÕES SOBRE OS ANIMAIS\n";
			for (int j = 0; j < proprietarios[i].quantidadeAnimais; j++) {
				string sexoAnimal = (proprietarios[i].animal[j].sexoAnimal == 'M') ? "Macho\n\n" : "Fêmea\n\n";
				if (proprietarios[i].animal[j].idadeAnimal != 0) {
					cout << "\tANIMAL (" << j + 1 << "): " << proprietarios[i].animal[j].nomeAnimal << endl
						 << "\tEspécie do animal: " << proprietarios[i].animal[j].especieAnimal  << " | Idade: " << proprietarios[i].animal[j].idadeAnimal << endl
						 << "\tCódigo do Animal: [ " << proprietarios[i].animal[j].codigoAnimal << " ] | Sexo do animal: " << sexoAnimal;  
				} 
			}
			if(proprietarios[i].quantidadeAnimais == 0) cout << "\n\tNENHUM ANIMAL CADASTRADO!\n\n";
		} 
	}
	if(contadorProp != 0) cout << "\n\tNENHUM PROPRIETÁRIO CADASTRADO\n\n";
}

void procurarProprietario(Proprietario *proprietarios, int n) {
	system("clear")||system("cls");
	int auxCPF, aux = 1000, contador=0;
	cout << "Qual CPF deseja Buscar?:";
	cin >> auxCPF;

	for (int i = 0; i < n; i++)
		if(auxCPF == proprietarios[i].CPF) 
			aux = i;

	if(aux != 1000) {
		cout << "PROPRIETÁRIO [" << aux + 1 << "]: " << proprietarios[aux].nome << " | Idade: " <<  proprietarios[aux].idade << " | CPF: " << proprietarios[aux].CPF << endl
			 << "TOTAL DE ANIMAIS: " << proprietarios[aux].quantidadeAnimais << endl
		     << "INFORMAÇÕES SOBRE OS ANIMAIS\n\n";

		for (int j = 0; j < proprietarios[aux].quantidadeAnimais; j++) {
			string sexovalido = (proprietarios[aux].animal[j].sexoAnimal == 'M') ? "Macho" : "Fêmea";
			if (proprietarios[aux].animal[j].idadeAnimal != 0) {
				cout << "\tANIMAL (" << j + 1 << ") | Nome: " << proprietarios[aux].animal[j].nomeAnimal << "| Idade: " << proprietarios[aux].animal[j].idadeAnimal << endl;
				cout << "\tEspécie do animal: " << proprietarios[aux].animal[j].especieAnimal <<  " | Sexo: " << sexovalido << " | Código do Animal: [ " << proprietarios[aux].animal[j].codigoAnimal << " ]\n\n";
			}else contador++;
		}
	} else cout << "\n\tNENHUM PROPRIETÁRIO COM ESSE CPF!\n";
	if(proprietarios[aux].quantidadeAnimais == 0) cout << "\n\tNENHUM ANIMAL CADASTRADO!\n\n";
}

void editarDeletar(Proprietario *proprietarios, int n) {
	system("cls")||system("clear");
	int  cpfchave, opc, aux=0, cpfXerox;//operacao  
	bool cpfExiste = false, flag=false;

	// Laço de verificação de cpf
	do {
		cout<<"\nDigite o CPF para procurar: ";	
		cin >> cpfchave;

		for(int i = 0; i < n ; i++) {
			if(cpfchave==proprietarios[i].CPF){
				aux  = i;
				cpfExiste = true;
			}
		}
	} while(cpfExiste == false);

	//Editar e Deletar
	do {
		cout << "| MENU DO PROPRIETÁRIO |\n"
			 << "| (1) - Editar         |\n"
		     << "| (2) - Excluir        |\n"
		     << "|----------------------|\n\n";
		cout << "Qual operacao deseja fazer: ";
		cin >> opc;

		switch (opc) {
		case 1:
			getchar();
			cout<<"Nome do Proprietario: ";
			getline(cin, proprietarios[aux].nome);
			cout << "Idade do proprietário: ";
			cin >> proprietarios[aux].idade;
			cout<<"\n\tPROPRIETÁRIO EDITADO COM SUCESSO!\n\n";
			break;
		case 2:
			proprietarios[aux].nome=" ";
			proprietarios[aux].idade = 0;
			proprietarios[aux].CPF=0;
			proprietarios[aux].quantidadeAnimais=0;
			cout<<"\n\tPROPRIETÁRIO DELETADO COM SUCESSO!\n\n";
			break;
		default:
			cout<<"\nOperacao Invalida!\n";
			break;
		}
	} while(opc != 1 && opc != 2 );

	if(cpfExiste != true) cout << "\n\tProprietário não encontrado!\n\n";
}

void procuraAnimais(Proprietario *proprietarios, int n) {	
	system("cls")||system("clear");
	string especie;
	int contadora=0;
	getchar();
	cout << "\n\nQual a espécie de animal deseja encontrar?";
	getline(cin, especie);
	cout<<"\n";
	for (int i = 0; i < n; i++){
		for (int j = 0; j < proprietarios[i].quantidadeAnimais; j++){
				if(especie==proprietarios[i].animal[j].especieAnimal) {
					contadora++;
					string sexovalido = (proprietarios[i].animal[j].sexoAnimal == 'M') ? "MACHO\n" : "FÊMEA\n";
					cout << "ANIMAL (" << j + 1 << ") | Nome do animal: " << proprietarios[i].animal[j].nomeAnimal << endl
					     << "Espécie do animal: " << proprietarios[i].animal[j].especieAnimal << " | Idade: " << proprietarios[i].animal[j].idadeAnimal << endl
						 << "Código animal: " << proprietarios[i].animal[j].codigoAnimal << " | Sexo do animal: " << sexovalido << endl;
				}
		}
	}
	if(contadora==0) cout << "\n\tNenhum animal dessa espécie!\n\n";
}

void editarAnimal(Proprietario *proprietarios, int n) {	
	system("cls")||system("clear");
	int cod=0, auxproprietario=0, auxAnimal=0;
	bool proprietarioExiste = false;
	
	cout << "\n\nQual o código do animal deseja encontrar?";
	cin>>cod;
	// pegar posições dos prop. e animal e verficar se existe um animal ;
	for (int i = 0; i < n; i++){
		for (int j = 0; j < proprietarios[i].quantidadeAnimais; j++){
			if(cod==proprietarios[i].animal[j].codigoAnimal) {	
			auxproprietario = i;
			auxAnimal =j;
			proprietarioExiste=true;
			}
		}
	}

	if(proprietarioExiste==1) {
		getchar();	
		cout << "Nome do animal: ";
		getline(cin, proprietarios[auxproprietario].animal[auxAnimal].nomeAnimal);
		cout << "Nome da especie: ";
		getline(cin, proprietarios[auxproprietario].animal[auxAnimal].especieAnimal);
		cout << "Idade do animal: ";
		cin >> proprietarios[auxproprietario].animal[auxAnimal].idadeAnimal;
		cout << "Sexo do animal(M/F): ";
		cin>> proprietarios[auxproprietario].animal[auxAnimal].sexoAnimal;

		cout << "\n\tAnimal editado com sucesso!\n\n" << endl;
	}else{
		cout<<"\n\tNenhum Animal listado para com esse código\n\n";
	}
}

void deletarAnimais(Proprietario *proprietarios, int n){
	system("cls")||system("clear");
	int auxCPF = 0, contadorAnimais = 0;
	bool cpfExiste = false;
	do {
		int contador = 0;
		cout << "CPF do Proprietário: ";
		cin >> auxCPF ;

		for (int k = 0; k < n; k++) {
			if (auxCPF == proprietarios[k].CPF) 
				contador++;
		}

		if (contador == 1)  cpfExiste = true;
		if (contador == 0) cout << "\n\tNão existe proprietário com esse CPF\n\n";
	} while (cpfExiste == false);

	for (int i = 0; i < n; i++){
		if(auxCPF==proprietarios[i].CPF){
			for (int j = 0; j < proprietarios[i].quantidadeAnimais; j++){
				proprietarios[i].animal[j].nomeAnimal=""; 
				proprietarios[i].animal[j].especieAnimal="";
				proprietarios[i].animal[j].idadeAnimal =0;
				proprietarios[i].animal[j].codigoAnimal=0;
				proprietarios[i].animal[j].sexoAnimal=' ';
				contadorAnimais++;
            }
		
		}
		proprietarios[i].quantidadeAnimais = 0;
	}
	if(contadorAnimais > 0) cout << "\n\tAnimais deletados com sucesso!\n\n";	
}

