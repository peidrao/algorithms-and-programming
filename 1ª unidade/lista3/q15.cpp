/*
 Uma loja tem, para cada um dos seus 10 funcionários, uma ficha contendo a identidade, o número de
horas trabalhadas e a quantidade de dependentes do mesmo. Considerando que:
a) A empresa paga 12 reais por hora e 40 reais por dependentes (salário bruto).
b) Sobre o salário bruto são feitos descontos de 8,5% para o INSS e 5% para o IR (salário líquido).
Elabore um algoritmo que leia os dados de todos os funcionários, calcule e escreva os salários bruto
(total) e líquido (total – descontos) de cada funcionário e a identidade de todos os funcionários com mais
de 3 dependentes.

 */

#include <iostream>
using namespace std;

int main() {
	int identidade[10], horas[10], dependentes[10];
	float salarioBruto[10], salarioLiquido[10];

	for (int i = 0; i < 10; i++)
	{
		cout << "Identidade do funcionário (" << i+1 << "): ";
		cin >> identidade[i];

		cout << "Horas trabalhadas do funcionário (" << i+1 << "): ";
		cin >> horas[i];
		salarioBruto[i] = horas[i] * 12; 

		cout << "Quantidade de dependentes do funcionário (" << i+1 << "): ";
		cin >> dependentes[i];

		if(dependentes[i] > 0) {
			salarioBruto[i] += (dependentes[i] * 40);
		}

		salarioLiquido[i] = salarioBruto[i] - ((salarioBruto[i] * 0.085) + (salarioBruto[i] * 0.05));
		cout << endl;
	}
	cout << endl;

	for (int i = 0; i < 10; i++)
	{
		cout << "Funcionário " << i + 1 << endl;
		cout << "Salário Bruto: R$" << salarioBruto[i] << endl;
		cout << "Salário Liquido: R$" << salarioLiquido[i] << endl;
		if(dependentes[i] > 3) {
			cout << "Identidade: " << identidade[i] << endl;
		}
		cout << endl;
	}



}
