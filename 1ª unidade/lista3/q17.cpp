#include <iostream>
#include <iomanip>

using namespace std;


int main () {
	char opc;
	int codProduto, quantProduto = 0;

	float valorProduto, totalProdutos = 0.0, valorPago = 0.0; 
	bool pago = false;

	do {
		cout << "Digite o código do produto: ";
		cin >> codProduto;
		cout << "Valor do produto: ";
		cin >> valorProduto;

		totalProdutos += valorProduto;
		quantProduto++;
		cout << "Deseja adicionar mais um produto: ";
		cin >> opc;


	} while(opc != 'n');

	cout << "Total de compras: R$ " << totalProdutos << endl
		 << "Quantidade de produtos: " << quantProduto << "\n\n";

	while (pago == false) {
		cout << "Digite o valor a ser pago: ";
		cin >> valorPago;

		if(valorPago == totalProdutos) {
			cout << "Valor pago com sucesso!" << endl;
			pago = true;
		} else if (valorPago < totalProdutos) {
			cout << "Valor insuficiente" << endl;
		} else if(valorPago > totalProdutos) {
			float troco = valorPago  - totalProdutos;
			cout << "Seu troco: R$ " << troco << endl;
			pago = true;
		}
	}
	cout << endl << "Volte sempre!" << endl;
}