#include <iostream>

using namespace std;

int main() {
	float chico = 1.5, juca = 1.1;
	int contador  = 0;
	do {
		chico += 0.2;
		juca += 0.3;

		cout << "Chico: " << chico << endl;
		cout << "Juca: " << juca << endl;
		
		contador ++;
	} while( juca < chico);

	cout<< endl << "Juca vai demorar " << contador << " anos para ser maior que Chico" << endl; 

}