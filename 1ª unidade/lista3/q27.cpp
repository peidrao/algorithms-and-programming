#include <iostream>
using namespace std;

   void capicua(int n)
    {
        int aux,soma;
        aux = n;
        soma = 0;
        
        while(aux != 0)
        {
             soma = soma * 10 + (aux % 10);
             aux = aux / 10;
        }
        if(soma == n)
         cout << "Este número é capicua." << endl;
        else
          cout << "Este número não é capicua." << endl;
    }

int main() {
	int numero;
	cout << "Digite um número: ";
	cin >> numero;

	capicua(numero);
}
