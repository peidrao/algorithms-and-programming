#include <iostream>
using namespace std;

int main() {
	int n, somaPrimos=0, numPrimo, verificarPrimo,
		t, contadorPrimos = 0, somarPrimos = 0;

	cout << "Quantos números deseja digitar? ";
	cin >> n;
	for (int i = 0; i < n; i++)
	{	cout << "Digie o número: ";
		cin >> numPrimo;
		t = 0;
		for (int j = 1; j <= numPrimo; j++){
			verificarPrimo = numPrimo % j;
			if(verificarPrimo == 0) {
				t+= 1;
			}
		}
		if(t == 2) {
			contadorPrimos += 1;
			somaPrimos += numPrimo; 
		}
	}

	cout << "A soma de todos os primos é " << somaPrimos << endl;
}
