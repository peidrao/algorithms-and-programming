#include <iostream>
#include <iomanip>

using namespace std;

int main() {
	int n1, n2;

	cout << "Digite um número: ";
	cin >> n1;
	n2 = n1;

	while(n2 > 0) {
		if(n1% n2 ==0) {
			cout << n2 << setw(4) ;	
		}
		n2--;
	}
cout << endl;
}

