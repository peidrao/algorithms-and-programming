#include <iostream>

using namespace std;

int main() {
	int numero,  total = 1;
	bool flag = false;
	cout << "Digite um número: ";
	cin >> numero;

	cout << numero << ":  ";
	for (int i = numero; i > 0 ; i--)
	{
		total*= i;
		if(i > 1) {
			cout << i << " * ";
		} else {
			cout << i  << " = ";
		}
	}
	cout << total;
	cout << endl;
}
