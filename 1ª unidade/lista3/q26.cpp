#include <iostream>

using namespace std;

int main() {
	float altura, alturaMaisBaixa =0, mediaAlturaHomem =0, somatorio =0; 
	int homens = 0, mulheres =0;
	char opc;


	do {
		cout << "Qual seu sexo (m=mascu/f=femin): ";
		cin >> opc;

		switch(opc) {
			case 'm':
			cout << "Digite sua altura: ";
			cin >> altura;
			mediaAlturaHomem += 0;
			homens++;
			break;
			case 'f':
			cout << "Digite sua altura: ";
			cin >> altura;
			if(alturaMaisBaixa < altura) 
				alturaMaisBaixa = altura;
			mulheres++;
			break;
			default:
			cout << "opção inválida!" << endl;
		}
		somatorio += altura;
		
	} while(altura != 'X' && altura != 'x');
	cout << endl;
	cout << "Menor altura das mulheres:  " << alturaMaisBaixa << endl
		 << "Média da altura dos homens: " << mediaAlturaHomem / homens << endl
		 << "Media de todas alturas:     " << somatorio / (homens+mulheres) << endl;
}
