#include <iostream>
#include <iomanip>
using namespace std;

int main() {
	int n[4];

	for (int i = 0; i < 4; i++)
	{
		cout << "Digite um número: ";
		cin >> n[i];

		 while(n[i] < 0) {
		 	cout << "Digite um número positivo: ";
			cin >> n[i];
		 }
	}


	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if( i== 0) {
				cout  << n[j] << setw(2) << " | " ;
			}
			else if(i ==1) {
				cout << n[j ] * n[j] <<  setw(2) << " | ";
			} else if(i==2) {
				cout << n[j] *  n[j] * n[j]<< setw(2) << " | ";
			}
		}
		cout << endl;
	}
	cout << endl;
}