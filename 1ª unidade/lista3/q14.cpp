/*
 	Carrinho de Picolé
 	Fazer um algoritmo que calcule a meta de picolés que precisam ser vendidos para que o vendedor possa 
 	lucrar. O programa deve receber os tipos de picolés (segundo o menu), a quantidade, e o preço. O programa 
 	termina quando sua meta for estipulada, ou seja se ele deseja uma meta de 500,00 reais ele precisa 
 	vender n tipos, quantidade e valores. Caso a meta não seja atingida, o programa deve calcular novamente.
 */


#include <iostream>
#include <math.h>

using namespace std;
 

int main() {
	int quantidadePicoles, totalPicoles = 0, tipoPicole;
	float precoPicoles, metaVendedor, total;
	bool metaBatida = false;
	float morango = 1, choco = 0.75, baun = 1.25, milho = 2, amen = 1.5, acai = 0.5;
	cout << "Qual a meta que deseja alcançar? "
	
	cin >> metaVendedor;

	cout << endl;

	do{
		cout << "|---------------------------------------|" << endl
			 << "|------ PCC: Picolé Caseiro Caicó ------|" << endl;
			 << "|------ TIPOS ------|------ PREÇO ------|" << endl
			 << "|1) Morango         |  R$ 1.00 unidade  |" << endl
			 << "|2) Chocolate       |  R$ 0.75 unidade  |" << endl
			 << "|3) Baunilha        |  R$ 1.25 unidade  |" << endl
			 << "|4) Milho           |  R$ 2.00 unidade  |" << endl
			 << "|5) Amendoin        |  R$ 1.50 unidade  |" << endl
			 << "|6) Açaí            |  R$ 0.50 unidade  |" << endl
			 << "|===================|===================|" << "\n\n";

		cout << "Qual o tipo de picole?\n>> ";
		cin >> tipoPicole;
		cout << "\n\n" ;
	switch(tipoPicole) {
		case 1: 
			cout << "Tipo escolhido: Morango | Preço R$ " << morango << endl
			cout << "Quantidade de picolés: ";
			cin >> quantidadePicoles;
			totalPicoles += quantidadePicoles;
			total += (morango * quantidade);
		break;
		case 2: 
		cout << "Tipo escolhido: Chocolate | Preço R$ " << choco << endl
			cout << "Quantidade de picolés: ";
			cin >> quantidadePicoles;
			totalPicoles += quantidadePicoles;
			total += (choco * quantidade);
		break; 
		case 3: 
		cout << "Tipo escolhido: Baunilha | Preço R$ " << baun << endl
			cout << "Quantidade de picolés: ";
			cin >> quantidadePicoles;
			totalPicoles += quantidadePicoles;
			total += (baun * quantidade);
		break; 
		case 4:
		cout << "Tipo escolhido: Milho | Preço R$ " << milho << endl
			cout << "Quantidade de picolés: ";
			cin >> quantidadePicoles;
			totalPicoles += quantidadePicoles;
			total += (milho * quantidade);
		break; 
		case 5: 
		cout << "Tipo escolhido: Amendoin | Preço R$ " << amen << endl
			cout << "Quantidade de picolés: ";
			cin >> quantidadePicoles;
			totalPicoles += quantidadePicoles;
			total += (amen * quantidade);
		break;
		case 6: 
		cout << "Tipo escolhido: Açaí | Preço R$ " << acai << endl
			cout << "Quantidade de picolés: ";
			cin >> quantidadePicoles;
			totalPicoles += quantidadePicoles;
			total += (acai * quantidade);
		break;  
		default:
		cout << "Tipo inválido, digite novamente!" << endl;
		break;
	}

		quantidadePicoles = 0;

		if(total >= metaVendedor) {
			metaBatida = true;
		} 

	} while(metaBatida == false);


	cout << "Você precisa vender: " << totalPicoles << " picolés" << endl;

}
