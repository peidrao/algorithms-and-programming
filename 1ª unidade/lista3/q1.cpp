#include <iostream>
#include <iomanip>
#include <ctime> 
using namespace std;

int main () {
    srand(time(NULL));

    int array[100];

    for (int i = 0; i < 100; i++)
    {
        array[i]= (rand() % 100 + 1);
    }

        int menorIdade = 10000;
        int maiorIdade = 0;
        for (int i = 0; i < 100; i++)
    {
        cout << endl;
            cout << setw(5) << array[i];
                if(array[i] < menorIdade) {
                menorIdade = array[i];
            } else if(array[i] > maiorIdade) {
                maiorIdade = array[i];
            }
        
    }
    cout << "\n\n";
    cout << "A maior idade é: " << maiorIdade << endl;
    cout << "A menor idade é: " << menorIdade << endl;
    cout << endl;

}