/*
  Escreva um algoritmo que auxilie o controle de entradas de um museu, dependendo da idade do
visitante. Considere que:
- Crianças com idade < 6 anos não pagam.
- Crianças de 6 a 12 anos pagam 30%.
- De 13 a 18 anos, e acima de 60 anos, pagam 50%.
- Os demais pagam 100%.
O algoritmo deve ler, inicialmente, o preço da entrada para aquele dia (sem desconto). Em seguida deve
solicitar a idade do visitante e informar o valor que ele deverá pagar. O algoritmo deve parar de solicitar
a idade do visitante quando for digitado 0 para a idade. Ao final, o algoritmo deverá escrever dois totais:
o total de dinheiro arrecadado e o total de desconto concedido pelo museu naquele dia.
 */

#include <iostream>
using namespace std;

int main() {
	float precoInicial = 0, totalArrecadado =0, totalDesconto = 0;
	int desconto=0;
	int idade;
	cout << "Valor do ingresso para esse dia: " ;
	cin >> precoInicial;

	do {
		cout << "Digite sua idade: ";
		cin >> idade;

		if(idade> 0 && idade < 6) {
			totalDesconto +=  precoInicial;
			desconto++;
		} else if(idade >= 6 && idade <= 12) {
			totalDesconto +=  precoInicial * 0.7;
			totalArrecadado += precoInicial * 0.3;
			desconto++;
		} else if((idade > 12 && idade <19) || idade > 60) {
			totalDesconto +=  precoInicial * 0.5;
			totalArrecadado += precoInicial * 0.5;
			desconto++;
		} else if (idade >= 19 && idade <= 60) {
			totalArrecadado += precoInicial;
		}

	} while(idade != 0);

	cout << "Total Arrecadado: R$ " << totalArrecadado << endl
		 << "Total de Descontos: R$ " << totalDesconto << endl
		 << "Quantidade de descontos: " << desconto << endl; 

}
