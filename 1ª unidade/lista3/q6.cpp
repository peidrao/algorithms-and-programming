#include <iostream>
#include <ctime> 

using namespace std;

int main () {
    srand(time(NULL));
	int numeroAleatorio = (rand() % 10 + 1);
    bool sucesso = false;	
    int contador = 0;
    int numero;

	//cout << numeroAleatorio << endl;

	do {
		cout << "Digite um número entre 1 e 10: ";
		cin >> numero;

		if(numero == numeroAleatorio) {
			cout << "Descobriu o número!" << endl;
			sucesso = true;
		} else {
			cout << "Número errado!" << endl;
		}

		contador++;
	} while((sucesso != true) && (contador < 3) ) ;
        
    if(sucesso) {
    	cout << endl << "você ganhou o jogo!" << endl;
    } else {
    	cout << "Você perdeu o jogo! O número correto era: " << numeroAleatorio << endl;
    }
}