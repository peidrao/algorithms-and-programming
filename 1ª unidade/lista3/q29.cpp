#include <iostream>
#include <ctime> 

using namespace std;
// Falta ordenação aleatória!
int main() {
	srand(time(NULL));
	int x[3], aux, opc, w, y, z;

	do {
		for (int i = 0; i < 3; i++){
			cout << "Digite um número: ";
			cin >> x[i];
			if(i ==0) w =x[i];
			if(i ==1) z =x[i];
			if(i ==2) y =x[i];
		}

		// Ordenação
		for (int i = 0; i < 3; i++){
			for (int j = i+ 1; j < 3; j++){
				if (x[i] > x[j]){
                aux = x[i];
                x[i] = x[j];
                x[j] = aux;
            	}
			}
		}

		cout << "+=== ORDEM DOS NÚMEROS ===+\n"
		     << "| 1) - Crescente          |\n"
		     << "| 2) - Descrescente       |\n"
		     << "| 3) - Aleatório          |\n"
		     << "+=========================+\n\n";
		cout << ">> ";
		cin >> opc;

		switch(opc) {
			case 1:
			cout << "Ordem Crescente:" << endl;
			for(int i = 0; i < 3; i++)
        		cout << x[i] << " ";
        	cout << endl;
			break;
			case 2:
			cout << "Ordem Descrescente:" << endl;
			for(int i = 2; i >= 0; i--)
        		cout << x[i] << " ";
        	cout << endl;
			break;
			case 3:
			cout << "Ordem aleatória:" << endl;
			// Falta!
			break;
			default:
			cout << "Ordem válida!" << endl;
		}

	} while(w!=0 && y != 0 && z != 0);
	cout << endl;
}
