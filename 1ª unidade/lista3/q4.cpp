#include <iostream>

using namespace std;

int main() {
	
	int numPerfeito=0,num;

	cout << "Digite um número: ";
	cin >> num;

	for(int i=1; i < num; i++){
		if(num%i==0) {
			numPerfeito += i;
		} 
	}
	
	if(numPerfeito == num ) {
		cout << "Número é perfeito!" << endl;
	} else {
		cout << "Número não é perfeito!" << endl;
	}
}