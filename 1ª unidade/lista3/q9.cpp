        #include <iostream>
    #include <ostream>
    #include <iomanip>

    using namespace std;

    void leafCalendar(int inputYear, int inputMonth);
    bool leafYear(int inputYear);
    int startToYear(int inputYear);
    int startToMonth(int inputYear, int inputMonth);
    int finishToDay[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    char display_year[5];
    char display_month[3];

    int main()
    {
        int inputYear, inputMonth, i;
        char response;
        char dayofWeek[7][5] = { {"Sun"},{"Mon"},{"Tue"},{"Wed"},{"Thu"},{"Fri"},{"Sat"} };
    do
    {
        cout << "enter the year and month. (exam: 2003 5) " << endl;   
        cin >> inputYear >> inputMonth;
        if (leafYear(inputYear))
            finishToDay[1] = 29;
        else finishToDay[1] = 28;
        cout << ("Input Month ");
        cout << (display_year, "$4d", inputYear);
        cout << ("Year");
        cout << (" ");
        cout << (display_month, "$2d", inputMonth);
        cout << ("Month");
        cout << ("is....") << endl;
        cout << ("\t< ");

        cout << (display_month, "$4d", inputYear);
        cout << (" ");
        if (inputMonth == 1) {
            cout << "January";
        };
        if (inputMonth == 2) {
            cout << "February";
        };
        if (inputMonth == 3) {
            cout << "March";
        };
        if (inputMonth == 4) {
            cout << "April";
        };
        if (inputMonth == 5) {
            cout << "May";
        };
        if (inputMonth == 6) {
            cout << "June";
        };
        if (inputMonth == 7) {
            cout << "July";
        };
        if (inputMonth == 8) {
            cout << "August";
        };
        if (inputMonth == 9) {
            cout << "September";
        };
        if (inputMonth == 10) {
            cout << "October";
        };
        if (inputMonth == 11) {
            cout << "November";
        };
        if (inputMonth == 12) {
            cout << "December";
        };
        cout << (" >");

        cout << "\n============================\n";
        if (inputMonth >= 1 && inputMonth <= 12)
        {
            for (i = 0; i < 7; i++)
                cout << " " << dayofWeek[i];
            cout << "\n----------------------------";
        }

        cout << endl;
        leafCalendar(inputYear, inputMonth);        
        cout << ("\n----------------------------\n");
        cout << "\n============================\n" << endl;
        cout << "Repeat?(Y/N): ";
        cin >> response;
    } while (response != 'N' || response != 'n');

    return 0;
}



    void leafCalendar(int inputYear, int inputMonth)
    {
        int StartToDay, LineBreak;

        int TermToLine = (startToYear(inputYear) + startToMonth(inputYear, inputMonth)) % 7;
        LineBreak = TermToLine;
        for (StartToDay = 0; StartToDay < TermToLine; StartToDay++)
            cout << "    ";

        for (StartToDay = 1; StartToDay <= finishToDay[inputMonth - 1]; StartToDay++)
        {   
            std::cout << "  " << std::setw(2) << std::setfill('0') << StartToDay;

            if (LineBreak == 6) {
                cout << "\n----------------------------" <<endl;
                LineBreak = 0;
            }
            else
                LineBreak++;

        }
    }

    bool leafYear(int b)
{
    if ((b % 4 == 0 && !(b % 100 == 0)) || (b % 400 == 0))
        return true;
    else {
        return false;
    }
}

int startToMonth(int inputYear, int inputMonth)
{
    int CheckToLeaf = 0;
    for (int i = 1; i < inputMonth; i++)
        CheckToLeaf += finishToDay[i - 1] % 7;
    if (inputMonth > 2 && leafYear(inputYear))
        CheckToLeaf++;

    return CheckToLeaf % 7;
}



int startToYear(int inputYear)
{
    int CheckToLeaf = 4;

    for (int a = 1980; a >= inputYear; a--)
    {
        CheckToLeaf += 6;
        if (leafYear(a))
            CheckToLeaf += 6;
    }    CheckToLeaf %= 7;

    return CheckToLeaf;
}