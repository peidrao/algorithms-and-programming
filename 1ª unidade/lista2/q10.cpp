#include <iostream>

using namespace std;

int main()
{
	float nota1, nota2, nota3, media;
	cout << "Nota do laboratorio: ";
	cin >> nota1;

	cout << "Avaliacao semestral: ";
	cin >> nota2;

	cout << "Exame final: ";
	cin >> nota3;

	media = (((nota1 * 2) + (nota2 * 3) + (nota3 * 5)) / (10));

	if (media >= 0 && media <= 2.9)
	{
		cout << "Reprovado!" << endl;
	}
	else if (media > 2.9 && media <= 4.9)
	{
		cout << "Recuperação!" << endl;
	}
	else if (media > 4.9)
	{
		cout << "Aprovado!" << endl;
	}
	else
	{
		cout << "Média inválida!" << endl;
	}
}
