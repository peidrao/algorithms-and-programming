#include <iostream>

using namespace std;

int main()
{

	int num1, num2, num3;
	char ordem;
	cout << "Digite tres numeros: " << endl;
	cout << "Numero 1: ";
	cin >> num1;

	cout << "Numero 2: ";
	cin >> num2;

	cout << "Numero 3: ";
	cin >> num3;

	cout << "Qual a ordem que deseja organizar os numeros: (c=crescente/d=decrescente)";
	cin >> ordem;

	switch (ordem)
	{
	case 'c':
		if (num1 < num2 && num1 < num3 && num2 < num3)
			cout << num1 << " " << num2 << " " << num3 << endl;
		else if (num1 < num2 && num1 < num3 && num2 > num3)
			cout << num1 << " " << num3 << " " << num2 << endl;
		else if (num2 < num1 && num2 < num3 && num1 < num3)
			cout << num2 << " " << num1 << " " << num3 << endl;
		else if (num2 < num1 && num2 < num3 && num3 < num1)
			cout << num2 << " " << num3 << " " << num1 << endl;
		else if (num3 < num1 && num3 < num2 && num1 < num2)
			cout << num3 << " " << num1 << " " << num2 << endl;
		else if (num3 < num1 && num3 < num2 && num2 < num1)
			cout << num3 << " " << num2 << " " << num1 << endl;
		break;
	case 'd':
		if (num1 > num2 && num1 > num3 && num2 > num3)
			cout << num1 << " " << num2 << " " << num3 << endl;
		else if (num1 > num2 && num1 > num3 && num2 < num3)
			cout << num1 << " " << num3 << " " << num2 << endl;
		else if (num2 > num1 && num2 > num3 && num1 > num3)
			cout << num2 << " " << num1 << " " << num3 << endl;
		else if (num2 > num1 && num2 > num3 && num3 > num1)
			cout << num2 << " " << num3 << " " << num1 << endl;
		else if (num3 > num1 && num3 > num2 && num1 > num2)
			cout << num3 << " " << num1 << " " << num2 << endl;
		else if (num3 > num1 && num3 > num2 && num2 > num1)
			cout << num3 << " " << num2 << " " << num1 << endl;
		break;
	default:
		cout << "ordem invalida!" << endl;
	}
}
