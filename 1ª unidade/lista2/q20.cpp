#include <iostream>

using namespace std;

void valorPlano(int idade)
{
  if (idade < 10)
    cout << "valor a ser pago: R$ 180,00" << endl;
  else if (idade >= 10 && idade <= 30)
    cout << "valor a ser pago: R$ 150,00" << endl;
  else if (idade >= 40 && idade <= 60)
    cout << "valor a ser pago: R$ 195,00" << endl;
  else if (idade > 60)
    cout << "valor a ser pago: R$ 230,00" << endl;
  else
    cout << "idade não está inclusa no plano de saúde!" << endl;
}

int main()
{
  int idade;

  cout << "Digite sua idade: ";
  cin >> idade;

  valorPlano(idade);
}