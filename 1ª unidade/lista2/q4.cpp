#include <iostream>

using namespace std;

int main()
{
	int ano;
	float preco;

	cout << "Digite o ano do seu carro: ";
	cin >> ano;

	cout << "Digite o preço do seu carro: ";
	cin >> preco;

	if (ano < 1990)
	{
		cout << "O imposto a pagar eh de: R$ " << (preco * (1 / 100)) << endl;
	}
	else
	{
		cout << "O imposto a pagar eh de: R$ " << (preco * (1.5 / 100)) << endl;
	}
}
