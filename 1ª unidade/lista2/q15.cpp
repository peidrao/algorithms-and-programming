#include <iostream>
#include <cmath>
using namespace std;

int main()
{
  float a, b, c, delta, raiz1, raiz2;

  cout << "Valor de a: ";
  cin >> a;

  cout << "Valor de b: ";
  cin >> b;

  cout << "Valor de c: ";
  cin >> c;

  if (a != 0)
  {
    delta = (b * b) - (4 * a * c);

    if (delta < 0)
    {
      cout << "sem raizes reais!" << endl;
    }
    else if (delta == 0)
    {
      raiz1 = (-b) / (2 * a);
      cout << "possui apenas uma raiz real: " << raiz1 << endl;
    }
    else
    {
      raiz1 = (-b - sqrt(delta)) / (2 * a);
      raiz2 = (-b + sqrt(delta)) / (2 * a);
      cout << "Raiz 1: " << raiz1 << endl;
      cout << "Raiz 2: " << raiz2 << endl;
    }
  }
  else
  {
    cout << "Nao eh uma equacao de segundo grau" << endl;
  }
}
