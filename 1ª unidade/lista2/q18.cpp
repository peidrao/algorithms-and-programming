#include <iostream>

using namespace std;

void verificarJuros(int parcelas, float total, float valorParcela)
{
  float valorDivido = parcelas * valorParcela;

  if (total == valorDivido)
  {
    cout << "Parcelamento sem juros!" << endl;
  }
  else
  {
    cout << "Parcelamento com juros" << endl;
  }
}

int main()
{
  int quantParcelas;
  float total, valorParcela;

  cout << "qual o total da compra: ";
  cin >> total;

  cout << "foi divido em quantas parcelas: ";
  cin >> quantParcelas;

  cout << "valor das parcelas: ";
  cin >> valorParcela;

  verificarJuros(quantParcelas, total, valorParcela);
}