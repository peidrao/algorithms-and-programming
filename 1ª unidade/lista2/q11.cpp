#include <iostream>

using namespace std;

int main()
{
  int n;
  int soma = 0;

  cout << "Digite um numero: ";
  cin >> n;

  if (n > 0)
  {
    while (n > 0)
    {
      soma += n % 10;
      n = (n / 10);
    }
    cout << "A soma dos algarismos eh: " << soma << endl;
  }
  else
  {
    cout << "Número inválido!" << endl;
  }
}