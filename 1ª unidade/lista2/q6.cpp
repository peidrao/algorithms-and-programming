#include <iostream>

using namespace std;

int main()
{
	int nivelProfessor, horas;

	cout << "Digite o nivel do professor: ";
	cin >> nivelProfessor;

	cout << "Horas de atuacao do professor: ";
	cin >> horas;
	cout << endl;

	switch (nivelProfessor)
	{
	case 1:
		cout << "Salário do professor nível 1: R$ " << 12.00 * horas << endl;
		break;
	case 2:
		cout << "Salário do professor nível 2: R$ " << 17.00 * horas << endl;
		break;
	case 3:
		cout << "Salário do professor nível 3: R$ " << 25.00 * horas << endl;
		break;
	default:
		cout << "Esse professor não está cadastrado!" << endl;
	}
}
