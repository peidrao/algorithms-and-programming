#include <iostream>

using namespace std;

int main()
{
  int num1, num2, num3;

  cout << "Digite tres numeros: " << endl;
  cin >> num1;
  cin >> num2;
  cin >> num3;

  if (num1 == num2 && num1 == num3 && num2 == num3)
    cout << "Todos os numeros sao iguais!" << endl;
  else if (num1 != num2 && num1 != num3 && num2 != num3)
    cout << "Todos os numeros sao diferentes!" << endl;
  else
    cout << "apenas dois numeros sao iguais!" << endl;
}