#include <iostream>
#include <string>

using namespace std;

int main()
{
	float altura;
	char sexo;

	cout << "Digite sua altura: ";
	cin >> altura;
	cout << "Digite seu sexo (h/m): ";
	cin >> sexo;

	if (sexo == 'h')
	{
		cout << "homem" << endl;
		cout << "Seu peso ideal é: " << (72.7 * altura) - 58 << " kg" << endl;
	}
	else if (sexo == 'm')
	{
		cout << "Seu peso ideal é: " << (62.1 * altura) - 44.7 << " kg" << endl;
	}
	else
	{
		cout << "Sexo inválido!" << endl;
	}
}