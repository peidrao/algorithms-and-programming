#include <iostream>

using namespace std;

int main()
{
  int litros;
  float km;

  cout << "Digite a distancia percorrida: ";
  cin >> km;

  cout << "Digite o combustivel gasto: ";
  cin >> litros;

  float total = km / litros;

  if (total < 8)
  {
    cout << "pode vender!" << endl;
  }
  else if (total >= 8 && total <= 12)
  {
    cout << "Economico!" << endl;
  }
  else if (total > 12)
  {
    cout << "Super economico!" << endl;
  }
}