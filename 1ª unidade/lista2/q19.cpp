#include <iostream>

using namespace std;

void verificarDia(int dia)
{

  switch (dia)
  {
  case 1:
  case 7:
    cout << "Fim de semana!" << endl;
    break;
  case 2:
  case 3:
  case 4:
  case 5:
  case 6:
    cout << "Dia util!" << endl;
    break;
  default:
    cout << "dia invalido" << endl;
  }
}

int main()
{
  int dia;

  cout << "Digite um dia: ";
  cin >> dia;

  verificarDia(dia);
}