#include <iostream>

using namespace std;

int antecessores(int n) {
	int total = n;
	for (int i = 0; i < 10 ; i++) {
		n--;
	 total += n;	
	}

	return total;
}

int sucessores(int n) {
	int total = n;
	for (int i = 0; i < 10 ; i++) {
		n++;
	 total += n;	
	}

	return total;
}

int main () {
	int numero;
	char letra;

	cout <<  "Digite um numero: " ;
	cin >> numero;

	cout << "Digite uma letra(a=antecessores/s=sucessores): " ;
	cin >> letra;
cout << endl;

	switch (letra) {
		case 'a':
		cout << "A soma de seus antecessores eh:  " << antecessores(numero) << endl;
		break; 
		case 's':
		cout << "A soma de seus sucessores eh:  " << sucessores(numero) << endl;
		break; 
		default: 
		cout << "Letra Inválida!" << endl;
	}
}
