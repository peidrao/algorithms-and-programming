#include <iostream>

using namespace std;

int main()
{
	int codigo;
	float salario;

	cout << "Digite seu codigo: ";
	cin >> codigo;

	cout << "Digite o seu salario: ";
	cin >> salario;
	cout << endl;

	switch (codigo)
	{
	case 101:
		cout << "Antigo salario: R$ " << salario << endl;
		cout << "Novo salario: R$ " << (salario * 0.1) + salario << endl;
		break;
	case 102:
		cout << "Antigo salario: R$ " << salario << endl;
		cout << "Novo salario: R$ " << (salario * 0.2) + salario << endl;
		break;
	case 103:
		cout << "Antigo salario: R$ " << salario << endl;
		cout << "Novo salario: R$ " << (salario * 0.3) + salario << endl;
		break;
	default:
		cout << "Antigo salario: R$ " << salario << endl;
		cout << "Novo salario: R$ " << (salario * 0.4) + salario << endl;
	}
}
