#include <iostream>

using namespace std;

void verify(int x)
{
	if (x % 2 == 0)
	{
		if (x > 100)
		{
			cout << "Esse número é par e é maior que 100" << endl;
		}
		else if (x == 100)
		{
			cout << "Esse número é par e igual  a 100" << endl;
		}
		else
		{
			cout << "Esse número é par, mas menor que 100" << endl;
		}
	}
	else
	{
		if (x >= 0)
		{
			cout << "Esse número é ímpar e positivo" << endl;
		}
		else
		{
			cout << "Esse número é ímpar, mas negativo" << endl;
		}
	}
}

int main()
{
	int x;

	cout << "Valor de X: ";
	cin >> x;
	verify(x);
}