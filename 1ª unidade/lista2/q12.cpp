#include <iostream>

using namespace std;

int main()
{
  int tempoServico, idade;

  cout << "Digite sua idade: ";
  cin >> idade;

  cout << "Digite seu tempo de servico: ";
  cin >> tempoServico;

  if (idade >= 65)
  {
    cout << "Pode se aposentar!" << endl;
  }
  else if (tempoServico >= 30)
  {
    cout << "Pode se aposentar!" << endl;
  }
  else if (idade >= 60 && tempoServico >= 25)
  {
    cout << "Pode se aposentar!" << endl;
  }
  else
  {
    cout << "Numero invalido!" << endl;
  }
}