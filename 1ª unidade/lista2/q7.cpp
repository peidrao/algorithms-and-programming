#include <iostream>

using namespace std;

int main()
{
	int sanduiche, suco, quantSanduiche, quantSuco;
	float valorSanduiche, valorSuco;

	cout << "codigo sanduiche: ";
	cin >> sanduiche;
	cout << "codigo do suco: ";
	cin >> suco;
	cout << endl;

	switch (sanduiche)
	{
	case 100:
		valorSanduiche = 1.10;
		break;
	case 101:
		valorSanduiche = 1.30;
		break;
	case 102:
		valorSanduiche = 1.50;
		break;
	case 103:
		valorSanduiche = 1.10;
		break;
	case 104:
		valorSanduiche = 1.30;
		break;
	default:
		cout << "Sanduiche indisponivel" << endl;
	}

	switch (suco)
	{
	case 105:
		valorSuco = 1.00;
		break;
	case 106:
		valorSuco = 2.00;
		break;
	case 107:
		valorSuco = 1.50;
		break;
	default:
		cout << "suco indisponivel" << endl;
	}

	cout << "Quantos  sanduiches: ";
	cin >> quantSanduiche;
	cout << "Quantos sucos:";
	cin >> quantSuco;

	cout << endl
			 << "O valor total do lanche eh: R$ " << (valorSanduiche * quantSanduiche) + (valorSuco * quantSuco) << endl;
}
