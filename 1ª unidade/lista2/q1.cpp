#include <iostream>

using namespace std;

void verify(int x, int y, int z)
{
	if ((x + y < z) || (x + z < y) || (z + y < x))
	{
		cout << "Isso não é um triângulo!" << endl;
	}
	else if ((x == y) && (x == z) && (z == y))
	{
		cout << "Triângulo Equilátero!" << endl;
	}
	else if ((x == y) || (x == z) || (y == z))
	{
		cout << "Triângulo Isósceles!" << endl;
	}
	else if ((x != y) && (x != z) && (y != z))
	{
		cout << "Truiângulo Escaleno" << endl;
	}
}

int main()
{
	int x, y, z;
	cout << "Digite três valores:" << endl;
	cout << "Valor de X: ";
	cin >> x;
	cout << "Valor de Y: ";
	cin >> y;
	cout << "Valor de Z: ";
	cin >> z;

	verify(x, y, z);
}