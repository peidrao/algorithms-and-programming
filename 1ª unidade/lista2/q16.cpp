#include <iostream>

using namespace std;

void maiorIdade(int n)
{

  if (n >= 18)
  {
    cout << "ja pode tirar a carteira" << endl;
  }
  else if (n < 0)
  {
    cout << "idade invalida!" << endl;
  }
  else
  {
    cout << "Faltam " << 18 - n << " anos para voce tirar a carteira!" << endl;
  }
}

int main()
{
  int idade;

  cout << "Digite sua idade: ";
  cin >> idade;

  maiorIdade(idade);
}