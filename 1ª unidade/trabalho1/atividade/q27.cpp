/*  Os números capicuas são aqueles que escritos 
da direita para esquerdaou da esquerda para 
direita tem o mesmo valor. Exemplo 929, 44, 97379. 
Fazer um algoritmo que, lido um número inteiro
positivo, calculee escreva se este é ou não capicua.
*/

#include <iostream>
using namespace std;

int main()
{
  int numero, compar, aux;

  cout << "Digite um número: ";
  cin >> numero;

  aux = numero; //recebe 515
  compar = 0;

  while (aux != 0)
  {
    compar = compar * 10 + (aux % 10);
    aux = aux / 10;
  }

  if (numero == compar)
  {
    cout << "é capicua" << endl;
  }
  else
  {
    cout << "não é capicua" << endl;
  }
}
