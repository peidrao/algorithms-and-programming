/* Elabore um algoritmo que apresente um menu contendo as opções 1-Triângulo, 2-Quadrado, 3-
Retângulo, 4-Trapézio, 5-Círculo, 6-Sair. 
Em seguida, de acordo com a opção escolhida pelo usuário, o
algoritmo deve solicitar as informações necessárias
 para cálculo da área, efetuar o cálculo e escrever o
resultado. O algoritmo só deverá finalizar
 quando o usuário escolher a opção 6-Sair.
 */

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
  int opc, baseMaior = 0, base = 0, altura = 0;

  do
  {
    cout << "|--------MENU--------|" << endl
         << "|1) Triângulo        |" << endl
         << "|2) Quadrado         |" << endl
         << "|3) Retângulo        |" << endl
         << "|4) Trapézio         |" << endl
         << "|5) Círculo          |" << endl
         << "|6) Sair             |" << endl
         << "|====================|"
         << "\n\n";

    cout << ">> ";
    cin >> opc;

    switch (opc)
    {
    case 1:
      cout << "Altura: ";
      cin >> altura;
      cout << "Base: ";
      cin >> base;
      cout << "Área do Triângulo é: " << base * altura << endl;
      break;
    case 2:
      cout << "Base: ";
      cin >> base;
      cout << "Área do Quadrado é: " << pow(base, 2) << endl;
      break;
    case 3:
      cout << "Altura: ";
      cin >> altura;
      cout << "Base: ";
      cin >> base;
      cout << "Área do Retângulo é: " << base * altura << endl;
      break;
    case 4:
      cout << "Altura: ";
      cin >> altura;
      cout << "Base Menor: ";
      cin >> base;
      cout << "Base Maior: ";
      cin >> baseMaior;
      cout << "Área do Trapézio é: " << ((base + baseMaior) * altura) / 2 << endl;
      break;
    case 5:
      float pi, raio;
      pi = 3.14;
      cout << "Raio: ";
      cin >> raio;
      cout << "Área do Círculo é: " << pow(raio, 2) * pi << endl;
      break;
    case 6:
      cout << "até logo! :)" << endl;
      break;
    default:
      cout << "opcao inválida!" << endl;
      break;
    }

  } while (opc != 6);
  cout << endl;
}
