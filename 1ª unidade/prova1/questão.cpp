/*
	Pedro Victor da Fonseca
 	Carrinho de Picolé
 	Faça um algoritmo que calcule a meta de picolés que precisam ser vendidos (dado um valor do tipo
 	flutuante) para que o vendedor possa lucrar. O programa deve receber a quantidade de picoles de 
 	cada tipo (segundo o menu). O programa termina quando o lucro calculado for maior ou igual a meta
 	determinada pelo vendedor. Ao encerrar, o programa deve informar o total de picoles e o lucro final .
 */
#include <iostream>
#include <iomanip>

using namespace std;
 
int calculo(int quantidadePicoles, float tipo, float total, int &totalPicoles ) {
	cout << "\nQuantidade de picolés: ";
	cin >> quantidadePicoles;
	totalPicoles += quantidadePicoles;
	for (int i = 0; i < quantidadePicoles; i++)
	{
		total += tipo;
	}
	return total;
}

int main() {
	int quantidadePicoles, totalPicoles = 0, tipoPicole;
	bool metaBatida = false;
	float metaVendedor, total = 0, acumulado = 0,
		morango = 1, choco = 0.75, baun = 1.25, milho = 2, amen = 1.5, acai = 0.5;

	cout << "Qual a meta que deseja alcançar? ";

	cin >> metaVendedor;
	cout << endl;

	do{
		cout << "|---------------------------------------|" << endl
			 << "|------ PCC: Picolé Caseiro Caicó ------|" << endl
			 << "|------ TIPOS ------|------ PREÇO ------|" << endl
			 << "|1) Morango         |  R$ 1.00 unidade  |" << endl
			 << "|2) Chocolate       |  R$ 0.75 unidade  |" << endl
			 << "|3) Baunilha        |  R$ 1.25 unidade  |" << endl
			 << "|4) Milho           |  R$ 2.00 unidade  |" << endl
			 << "|5) Amendoin        |  R$ 1.50 unidade  |" << endl
			 << "|6) Açaí            |  R$ 0.50 unidade  |" << endl
			 << "|===================|===================|" << "\n\n";

		cout << "Qual o tipo de picole?\n>> ";
		cin >> tipoPicole;

	switch(tipoPicole) {
		case 1: 
			total += calculo(quantidadePicoles, morango, acumulado,totalPicoles); 
		break;
		case 2: 
			total += calculo(quantidadePicoles, choco, acumulado, totalPicoles); 
		break; 
		case 3: 
			total += calculo(quantidadePicoles, baun, acumulado, totalPicoles); 
		break; 
		case 4:
			total += calculo(quantidadePicoles, milho, acumulado, totalPicoles); 
		break; 
		case 5: 
			total += calculo(quantidadePicoles, amen, acumulado, totalPicoles); 
		break;
		case 6: 
			total += calculo(quantidadePicoles, acai, acumulado, totalPicoles); 
		break;  
		default:
		cout << "Tipo inválido, digite novamente!" << endl;
		break;
	}
		quantidadePicoles = 0;
		
		if(total >= metaVendedor) {
			metaBatida = true;
		} 

	} while(metaBatida == false);

	cout << "Você precisa vender: " << totalPicoles << " picolés" << endl
		 << "Lucro: R$ "  << fixed << setprecision(2) << total << endl;

}
